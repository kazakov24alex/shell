CC=g++
CFLAGS=-c
INC=-I./

SOURCES = $(wildcard src/*.cpp)
OBJECTS = $(SOURCES:src/%.cpp=obj/%.o)
EXECUTABLE=shell
INSTALL_PATH?=/usr


all: dirs $(EXECUTABLE)

dirs:
	@mkdir -p obj

$(EXECUTABLE): $(OBJECTS)
	@echo Linking sesh...
	$(CC) $(OBJECTS) -o $@
	@echo Linking is complete

$(OBJECTS): obj/%.o: src/%.cpp
	@echo Compiling $<...
	$(CC) $< -c -o $@ $(INC)

clean:
	@echo Cleaning...
	@rm -rf obj
	@rm -rf $(EXECUTABLE)
	@echo Cleaning is complete


install: all
	@echo Installing...
	@cp ./shell $(INSTALL_PATH)/bin
	@mkdir -p $(INSTALL_PATH)/share/shell
	@cp share/help $(INSTALL_PATH)/share/shell
	@cp share/version $(INSTALL_PATH)/share/shell
	@cp share/shell.1 $(INSTALL_PATH)/share/man/man1
	@echo Installing is complete

uninstall: all
	@echo Uninstalling...
	@rm -f $(INSTALL_PATH)/bin/shell
	@rm -f $(INSTALL_PATH)/share/shell/help
	@rm -f $(INSTALL_PATH)/share/shell/version
	@rmdir $(INSTALL_PATH)/share/shell
	@rm -f $(INSTALL_PATH)/share/man/man1/shell.1
	@echo Uninstalling is complete