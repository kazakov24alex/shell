#include "inc/repl.h"

#include <cstdio>
#include <iostream> 
#include <unistd.h> 
#include <cstring>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
using namespace std;

#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define MAX_FILENAME 256
#define MAX_PATH 1024
#define MAX_CMD_LEN 256

enum enumcom {LS, CD, HISTORY, HELP, MKDIR, TOUCH, CAT, FIL, CHMOD, CHOWN, CHGRP};



/// The finction provides showing choices of autocomplete
void Repl::autocomplete()
{       
    char** suggestions;
    int slash = 0;  // position of slash in the argument for autocompletion
    bool cd = false; // indicator of 'cd' command
    
    /// parse arguments to get a piece of autocompletion
    strcpy(buf_copy, buf);
	char* argv[MAX_FILENAME];
	int argc = parser_arg(buf_copy, argv);
    
    /// not autocomplete empty command
    if(argc == 0) {
        // ls_cmd();
        return;
    }
    
    /// is it command?
    int NUM_CMD = 11;
    char typecom[][MAX_CMD_LEN] = {"ls", "cd", "history", "help", "mkdir","touch", "cat", "file", "chmod", "chown", "chgrp"};    
    for(int i = 0; i < 11; i++) {
        if(!strcmp(argv[0], typecom[i])) {
            cd = true;
        }
    }

    /// detect the presence of a slash in the path and its position
    int len_arg = strlen(argv[argc-1]);
    for(int i = 0 ; i < len_arg; i++) {
        if (*(argv[argc-1]+i) == '/') {
            cd = true;
            slash = i;
        }
    }    
   
    /// slash is present      
    if(slash != 0) {
        /// separate the piece to complement him on the way up
        *(argv[argc-1]+slash) = '\0';

        /// go to the directory of the piece
        char** arguments = new char* [2];
        char name_cur_dir[MAX_FILENAME];
        getcwd(name_cur_dir, MAX_FILENAME);
        arguments[1] = argv[argc - 1];

        if(!cd_cmd(2, arguments))
            return;
        /// are looking for appropriate options for autocompletion
        suggestions = autocomplete_find((argv[argc-1]+slash+1), cd);
        /// go back to the current directory
        arguments[1] = name_cur_dir;        
        cd_cmd(2, arguments);
        *(argv[argc-1]+slash) = '/';
    }   
    /// slash is absent 
    else
        /// just looking for a piece of complement
        suggestions = autocomplete_find((argv[argc-1]), cd);
    
    /// If there is more than one option, 
    //# it lists all the autocompletion options
	if ((suggestions[0] != NULL && suggestions[1] != NULL)) {
	    int i = 0;
	    write(STDOUT_FILENO, "\n", 1);
	    while (suggestions[i] != NULL) {
            cout << "  " << suggestions[i++] << endl;
	    }
	    print_cwd();
        write(STDOUT_FILENO, buf, strlen(buf));
    } 
    /// If there is the only one option, 
    //# supplement the existing piece in the input line
    else if (suggestions[0] != NULL) {
        write(STDOUT_FILENO, "\n", 1);
		print_cwd();
        
        cur = buf + len;
        if(slash == 0)
            cur -= strlen(argv[argc-1]);
        else
            cur -= strlen(argv[argc-1])-slash-1;
            
        while ((*cur++ = *suggestions[0]++) != 0);
		cur--;
        
		write(STDOUT_FILENO, buf, strlen(buf));
        len = strlen(buf);
	}
};  


/// The function search autocomplitons to the piece in the CWD and/or PATH
char** Repl::autocomplete_find(char* piece, bool cd) {
    struct dirent *entry;
	char name_dir[MAX_FILENAME];    
    
	int Wcounter = 0;  // counter of symbols in the autocomplete array
	int Scounter = 0;  // counter of content in the autocomplete array

    /// get PATH
	char path[MAX_PATH];
    int pathlenOrig;   // length of original PATH. if PATH is used
	int i = 0;         // length of current woking directory address
    
    /// autocompletion only from CWD, if this is 'cd' command
    if (cd == true) {
        getcwd(path, MAX_FILENAME);
        //cout << "AC: path=" << path << "     piece=" << piece << endl;
        pathlenOrig = strlen(path);
    }   
    /// autocompletion from CWD and PATH, if this isn't  'cd' command
    else {    
        char* env = getenv("PATH");
        strcpy(path, env);
        pathlenOrig = strlen(path);  // length of original PATH

        /// add current working directory in path construction
	    *(path+pathlenOrig) = ':';
        getcwd(name_dir, MAX_FILENAME);
    
	    while (name_dir[i] != '\0') {
            *(path+pathlenOrig+i+1) = name_dir[i];
		    i++;
	    }
        *(path+pathlenOrig+i+1) = '\0';
    }
        
    /// replace the colon by zeros
    int start = 0; // point of new address in the PATH constuction
	int pathlen = strlen(path);    // length of new PATH
	for(i = 0; i <= pathlen; i++) {
		if ((*(path+i) == ':') || (*(path+i) == '\0')) {
            *(path+i) = '\0';
        
            /// extract the contents of a directory
		    DIR *dir = opendir(path+start);

            /// continue when there is no content
            if(dir == NULL) 
                continue;
            /// read the contents of the folder one by one
		    while((entry = readdir(dir)) != NULL) {
                int i = 0;
			    int notPrefix = 0;  /// Is peice prefix of thecontent?
            
                /// try to understand whether our is fragment prefix the content directory
			    while((piece[i] != '\0')) {
				    if(entry->d_name[i] != piece[i]) {
					   notPrefix = 1;
					   break;
				}
				if(entry->d_name[i] == '\0') {
					notPrefix = 1;
					break;
				}
				i++;
			}
            /// the piece is a prefix, write it in the array
            if(notPrefix == 0) {
				autocomplete_array[Wcounter++] = autocomplete_buf+Scounter;
				i = 0;
				while(entry->d_name[i]!=0) {
					autocomplete_buf[Scounter++] = entry->d_name[i++];
				}
				autocomplete_buf[Scounter++] = '\0';
			}
		}
		closedir(dir);
        	
		*(path+i) = ':';
		start = i + 1;
		}
	}
	*(path+pathlenOrig) = '\0';
	
    autocomplete_array[Wcounter] = NULL;

	return autocomplete_array;
};