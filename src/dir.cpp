#include "inc/repl.h"

#include <cstdio>
#include <iostream> 
#include <unistd.h> 
#include <cstring>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
using namespace std;

#define MAX_FILENAME 256
#define MAX_PATH 1024
#define HOW_MUCH_TABS 4
#define UMASK 0777


/// The function moves user to the catalogue, that they enter. 
bool Repl::cd_cmd(int argc, char** argv) {
	char* dir;
    
    /// if 'cd' hasn't an argument, move to the home directory
	if (argc == 1) {
		dir = getenv("HOME");
    /// if 'cd' has an argument, move to the home directory from argument
	} else {
		dir = argv[1];
	}
    
    /// check error
	int err = chdir(dir);
	if (err != 0) {
        if(strcmp(argv[0], "cd") == 0)
            cout << "  Unable to change directory, error: " << err << endl;  
            return false;
    }
    return true;
}


/// The function creates new folder on the CWD
void Repl::mkdir_cmd(int argc, char** argv) {
    /// get the CWD
    char cwd[MAX_FILENAME];
    getcwd(cwd, MAX_FILENAME);
    strcat(cwd, "/");

    /// process each argiments of the command
    for(int i = 1; i < argc; i++) {
        /// make absolute path of new folder
        char path[MAX_FILENAME];
        strcpy(path, cwd);
        strcat(path, argv[i]);     
        
        /// create new folder   
        int err = mkdir(path, UMASK);
        if (err == 0)
            cout << "  folder '" << argv[i] << "' was created" << cwd << endl;
        else
            cout << "  while creating '" << argv[i] << "', an error occurred" << endl;       
    }
}


/// The function of outputting of history from file. 
void Repl::touch_cmd(int argc, char** argv) { 
    /// get the CWD
    char cwd[MAX_FILENAME];
    getcwd(cwd, MAX_FILENAME);
    strcat(cwd, "/");
    
    /// create new file
    for(int i = 1; i < argc; i++) {
        char path[MAX_FILENAME];
        strcpy(path, cwd);
        strcat(path, argv[i]);
        ofstream file(path, ios_base::trunc);
        if(file.is_open())
            cout << "  file '" << argv[i] << "' was created in " << cwd <<endl;
        else
            cout << "  while creating '" << argv[i] << "', an error occurred" << endl;       
        file.close();
    }
}


/// The function prints the contents of the specified folder
void Repl::ls_cmd(int argc, char** argv) {
	struct dirent *entry;
	struct stat st;
    
    bool attributes = false;
	
	char name_dir[MAX_FILENAME];
	char filename[MAX_FILENAME];
    char full_name[MAX_FILENAME];
	
    for(int i=0; i < argc; i++)
        if(!strcmp(argv[i],"-a"))
            attributes = true;
    
    
    /// if argc 1, print the contents of the CWD
	if (argc==1 || (argc == 2 && attributes==true)) {
		getcwd(name_dir, MAX_FILENAME);
    /// else print the contents of a path from the argument
	} else {
		if (argv[1][0] == '/') {
			strcpy(name_dir, argv[1]);
		} else {
			getcwd(name_dir, MAX_FILENAME);
			strcat(name_dir, "/");
			strcat(name_dir, argv[1]);
		}
	}

    /// open the source directory
	DIR *dir=opendir(name_dir); 
	/// directory isn't opened - print error message
    if (!dir) {
        cout << "  There is no such directory: " << argv[1] << endl;
	}
    /// directory is opened - read conents
    else {
        if(argc != 1) {
            strcpy(name_dir, argv[1]);
            strcat(name_dir, "/");
        }
        /// read and print a list of the contents to the end, skipping directories '.' and '..'
        while ( (entry = readdir(dir) ) != NULL) {
			if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
                continue;
			
			strcpy(full_name, name_dir);
			strcat(full_name, "/");
			strcat(full_name, entry->d_name);
			
            /// read attributes
			if(lstat(full_name, &st)){
                /// attributes not read - print error message
				cout << "  " << entry->d_name << " unable to define attributes" << endl;
			} 
            /// attributes was read -  print the content with them
            else {
                /// print a name of the file
                cout << "\t" << entry->d_name;
                
                /// set tabulation (considering names of full two-bytes type)
                int tb = 1;  // the number of bytes in the first character
                if (entry->d_name[0] == -48 || entry->d_name[0] == -47)
                    tb = 2;
                int tab_len = (strlen(entry->d_name)/tb)/8;  // as much as tabs in a word
                for(int i = 0; i < (HOW_MUCH_TABS-tab_len); i++)
                    cout << "\t";
                
                /// print attributes of the content
                switch(st.st_mode & S_IFMT){
					case S_IFLNK:
                        cout << "(link)" << endl;
                        break;
					case S_IFDIR:
                        cout << "(directory)" << endl;
                        break;
					case S_IFREG:
						cout  << "(regular)" << endl;
                        break;
					case S_IFCHR:
						cout  << "(device)" << endl;
                        break;
					case S_IFBLK:
                        cout << "(device)" << endl;
                        break;
					case S_IFIFO:
                        cout << "(FIFO)" << endl;
                        break;
					case S_IFSOCK:
                        cout << "(socket)" << endl;
                        break;
					default:
                        cout << "(undefined type)" << endl;
                        break;
				}
			}	
		}
	}
	closedir(dir);
}

