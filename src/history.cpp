#include "inc/repl.h"

#include <cstdio>
#include <iostream>
#include <unistd.h>
#include <cstring>
#include <stdlib.h>
#include <fstream>
#include <cmath>

#define HISTORY_SIZE 500
#define MAX_CMD_LEN 256
#define MAX_FILENAME_LEN 256

#define KEY_SC_UP 'A'
#define KEY_SC_DOWN 'B'
#define EOL '\n'


///	THE FUNCTION adds command in history
/// RETURNS nothing
void Repl::history_save_cmd(char* cmd) {
    char *buf = new char [MAX_CMD_LEN];
    strcpy(buf, cmd);
    
    /// history buffer overflow protection
    if(hist_count > (HISTORY_SIZE*2-10))
        history_init();
        
    ofstream f_update(history_file, ios_base::app);
    f_update << buf;
    f_update << EOL;
    f_update.close();
    
    history_entries[hist_count++] = buf;
}


/// The FUNCTION provides opening the history file of commands, unloading commands 
//# from it and cuts history file of commands to the declared volume
/// RETURNS nothing
void Repl::history_init() {
    /// opening the history file in your home directory
    hist_count = 0;
	const char *home = getenv("HOME");
	strcpy(history_file, home);
	strcat(history_file, "/.shell_history");

    /// counting commands in the history file
    int lines_num = 0;
    char *cmd = new char [MAX_CMD_LEN];
    ifstream f_line(history_file);
    if(f_line.is_open()) {
        while (!f_line.eof()) {
            f_line.getline(cmd, MAX_CMD_LEN, '\n');
            lines_num++;
        }
    }
    f_line.close();
    
    /// skipping obsolete commands
    ifstream f_read(history_file);
    for(int i = 0; i < (lines_num - HISTORY_SIZE - 1); i++)
        f_read.getline(cmd, MAX_CMD_LEN);
    delete cmd;
    
    /// unloading part of commands claimed volume
    while(!f_read.eof()) {
        char* cmd = new char [MAX_CMD_LEN];
        f_read.getline(cmd, MAX_CMD_LEN);
        history_entries[hist_count++] = cmd;

    }
    hist_count--;  /// skipping the empty last line
        
    /// clearing the history file
    ofstream f_clear(history_file, ios_base::trunc);
    f_clear.close();
    
    /// loading claimed numbers of command in the history file
    ofstream f_update(history_file, ios_base::app);
    char *com = new char [MAX_CMD_LEN];
    for(int i = 0; (i < HISTORY_SIZE) && (i < hist_count); i++) {
        strcpy(com, history_entries[i]);
        f_update << com;
        f_update << EOL;
    } 
    delete com;
    f_update.close();
}


/// THE FUNCTION shows outputting of history from file
/// RETURNS nothing
void Repl::history_cmd(int argc, char** argv) {   
    int num = 0;  
    /// check on argument of size of output history block
    if(argv[1] != NULL) {
        for(int i = 0; i < strlen(argv[1]); i++) {
            if(!(argv[1][i] >= '0' && argv[1][i] <= '9')) {
                cout << "  syntax error (the argument isn't number)" << endl;
                return;
            }
        }
        int ten = strlen(argv[1]) - 1;
        for(int i = 0; i < strlen(argv[1]); i++)
            num = num + ((int)argv[1][i] - 48) * pow(10,ten--);
        
        if(hist_count < num) {
            cout << "  history contains less number of commands (" << hist_count << ")" << endl;  
            return;
        }
        num = hist_count - num;
    }
    
    /// print history block
    cout << "  Location history file: " << history_file << endl;
    cout << "  The last " << hist_count - num << " entered commands:" << endl;  
    for(int i = num; i < hist_count; i++)
        cout << "    "<< i+1 << "   \t" << history_entries[i] << endl;
}


/// THE FUNCTIONprovides lifting a command from the history to the input line
/// RETURNS nothing
void Repl::history_lift(int key)
{
    /// was pressed up arrow: it gives the following command
    if (key == KEY_SC_UP) {
        command = history_entry(history_index);
        
        if (command != NULL) {
            history_index++;
            replace_buf(command);           
		} else
            history_index--;
    } 
    /// was pressed down arrow: it gives the previous command
    else {
        if (history_index <= 0) {
            replace_buf("");
		} else {
            history_index--;
            command = history_entry(history_index);
			if (command != NULL) {
			    replace_buf(command);
            }
        } 
    }
}


/// THE FUNCTION gives the command corresponding to a particular 
//# index from the history array
/// RETURNS pointer of the search string
char* Repl::history_entry(int i) {
	if ((hist_count-i-1) >= 0) { 
        return history_entries[hist_count-i-1];
	} else {
		return NULL;
	}
}