#include "inc/repl.h"

#include <cstdio>
#include <iostream> 
#include <unistd.h> 
#include <cstring>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <cmath>
#include <fstream>
using namespace std; 


#define BUFFER_SIZE 256     // the number of arguments in the line

#define MAX_PATH 1024
#define MAX_LINE_LEN 256
#define MAX_VAR_NAME 256
#define MAX_VAR_LEN 9

#define SUM 43  // ASCII code of '+'
#define SUB 45  // ASCII code of '-'
#define MUL 42  // ASCII code of '*'
#define DIV 92  // ASCII code of '\'
#define BEG 40  // ASCII code of '('
#define END 41  // ASCII code of ')'

#define SIGN 1      // type of signs+
#define NUM 2       // type of number
#define VAR 3       // type of variable
#define P_OP 4      // type of opening parenthesis+
#define P_CL 5      // type of closing parenthesis+

#define EQUAL 6     // type of '=='+
#define NOT_EQUAL 7 // type of '!='+
#define MORE 8      // type of '>'+
#define LESS 9      // type of '<'+
#define MORE_EQ 10  // type of '>='+
#define LESS_EQ 11  // type of '<='+

#define AND 12      // type of '&&"+
#define OR 13       // type of '||'+
#define NOT 14      // type of '!'+

#define LOG 15     // type of logic 


/// THE FUNCTION calculate a value of the expression
//# RETURN a value of the expression in char* form
//# or NULL - if the expression incorrent
char* Repl::logic_calculator(int argc, char** argv)
{
    int type[BUFFER_SIZE]; // map of tupes of math components
    /// check the syntax of the mathematical expression
    //# if expression is incorrect - return NULL
    if(logic_map(argc, argv, type) != true)
        return NULL;
    
    if(logic_syntax(argc, argv, type) != true)
        return NULL;
        
    if(math_substitution(argc, argv, type) != true) 
        return NULL;
        
   
    return logic_calculation(argc, argv, type); 
};


/// THE FUNCTION calculate a value of the exression
//# RETURN a value of the expression in int form
char* Repl::logic_calculation(int argc, char** expr, int* type)
{    
    char** subexpr = new char* [BUFFER_SIZE];
    
    bool math_flag = true;    // flag of math SIGN
    /// while there are math SIGN
    while(math_flag == true) {
        math_flag = false;
        /// for each component
        for(int i = 0; i < argc; i++) {
            if(type[i] == SIGN) {
                math_flag = true;
                int j = i + 1;
                int opened = 0;
                int closed = 0;
                
                while(i >= 0) {
                    if (type[i] == P_OP) {
                        //closed++;
                        break;
                    }
                    i--;
                        /*
                    if (type[i] == P_OP)
                        if (closed == 0) {
                            cout << i << "opened" << endl;
                            opened++;
                            break;
                        }
                        else
                            closed--;
                    i--;
                    */
                }
                
                
                while(j < argc && (type[j]==SIGN || type[j]==NUM || type[j]==VAR || type[j]==P_OP || type[j]==P_CL)) {
                    if (type[j] == P_OP)
                        opened++;
                    if (type[j] == P_CL)
                        if (opened == 0) {
                            closed++;
                        }
                        else
                            opened--;
                    j++;
                }
                
                j--;
                if(j == (argc-1))
                    j--;
                
                
                if(i<0)
                    i=0;
                if(j>=argc)
                    j=argc-1;
                
                

                //char** subexpr = new char* [j-i+1];
                int subtype[j-i+1];
                int subargc = 0;
                for(int k = i; k <= j; k++) {
                    subexpr[subargc] = expr[k];
                    subargc++;
                }
                subexpr[subargc] = NULL;
                
                /*
                int l = 0;
                cout << "  " << i << "  " << j << endl;
                while(l < subargc)
                    cout << "--" << subexpr[l++];
                cout << endl; 
                */
                
                int res = math_calculator(subargc, subexpr);
                //expr[i] = subexpr[0];
                strcpy(expr[i], math_tostring(res));
                type[i] = NUM;

                for(int k = j+1; k < argc; k++) { 
                    expr[k-(j-i)] = expr[k];
                    type[k-(j-i)] = type[k];
                }
                argc = argc - (j-i);                
                expr[argc+1] = NULL; 
                
                //delete[] subexpr;
                
            }
        }
    }
  


    bool parenthess_flag = true;
    bool negation_flag = true;
    /// while there are parenthesses or negations
    while(parenthess_flag==true || negation_flag==true) {
        parenthess_flag = false;
        negation_flag = false;
        /// for each component
        for(int i = 0; i < argc; i++) {
            /// if found opening parenthess, then we look for the closing
            //# parenthess and send the contents of the parentheses in the
            //# log_calculation() 
            if(type[i] == P_OP) {
                parenthess_flag = true;
                int j = i + 1;
                int opened = 0; 
                while(j < argc) {
                    if (type[j] == P_OP)
                        opened++;
                    if (type[j] == P_CL)
                        if (opened == 0)
                            break;
                        else
                            opened--;
                    j++;
                }
                
                char** subexpr = new char* [j-i-1];
                int subtype[j-i-1];
                int subargc = 0;
                for(int k = i+1; k < j; k++) {
                    subexpr[subargc] = expr[k];
                    subtype[subargc] = type[k];
                    subargc++;
                }
                    
                logic_calculation(subargc, subexpr, subtype);
                
                expr[i] = subexpr[0];
                //strcpy(expr[i],"T");
                type[i] = LOG;

                for(int k = j+1; k < argc; k++) { 
                    expr[k-(j-i)] = expr[k];
                    type[k-(j-i)] = type[k];
                }
                argc = argc - (j-i);
                expr[argc] = NULL;  
                delete[] subexpr;            
            }     
     
            /// if found negation, we send the contents of negation in the
            //# log_calculation()
            if(type[i] == NOT) {
                negation_flag = true;
                if(type[i+1]==P_OP) {
                    i++;
                    int j = i + 1;
                    int opened = 0; 
                    while(j < argc) {
                        if (type[j] == P_OP)
                            opened++;
                        if (type[j] == P_CL)
                            if (opened == 0)
                                break;
                            else
                                opened--;
                        j++;
                    }
                    
                    char** subexpr = new char* [j-i-1];
                    int subtype[j-i-1];
                    int subargc = 0;
                    for(int k = i+1; k < j; k++) {
                        subexpr[subargc] = expr[k];
                        subtype[subargc] = type[k];
                        subargc++;
                    }
                    
                    logic_calculation(subargc, subexpr, subtype);
                    expr[i] = subexpr[0];
                    type[i] = LOG;
                    
                    for(int k = j+1; k < argc; k++) { 
                        expr[k-(j-i)] = expr[k];
                        type[k-(j-i)] = type[k];
                    }
                    argc = argc - (j-i);
                    expr[argc] = NULL;  
                                       
                } 
                if(type[i+1]==LOG) {
                    argc = negat_operation(i, argc, expr, type);
                    expr[argc] = NULL;
                }
            }
        } 
    }
    
    
   
   
    bool morles_flag = true;    // flag of MORE, MORE_EQ, LESS, LESS_EQ
    /// while there are MORE, MORE_EQ, LESS, LESS_EQ
    while(morles_flag == true) {
        morles_flag = false;
        /// for each component
        for(int i = 0; i < argc; i++) {
            /// if this is  MORE, MORE_EQ, LESS, LESS_EQ -> compar_operation()
            if(type[i]==MORE || type[i]==MORE_EQ || type[i]==LESS || type[i]==LESS_EQ) {
                morles_flag = true;
                argc = compar_operation(i, argc, expr, type);
            }
        }    
    }
    
    
    bool equal_flag = true;    // flag of EQUAL and NOT_EQUAL
    /// while there are EQUAL and NOT_EQUAL
    while(equal_flag == true) {
        equal_flag = false;
        /// for each component
        for(int i = 0; i < argc; i++) {
            /// if this is  EQUAL and NOT_EQUAL -> compar_operation()
            if(type[i]==EQUAL || type[i]==NOT_EQUAL) {
                equal_flag = true;
                argc = compar_operation(i, argc, expr, type);
                expr[argc] = NULL;
            }
        }    
    }

    
    bool and_flag = true;    // flag of AND
    /// while there are AND
    while(and_flag == true) {
        and_flag = false;
        /// for each component
        for(int i = 0; i < argc; i++) {
            /// if this is AND -> logic_operation()
            if(type[i]==AND) {
                and_flag = true;
                argc = logic_operation(i, argc, expr, type);
                expr[argc] = NULL;
            }
        }    
    }
/*    
            cout << "<<<<<   ";
    for(int i = 0; i < argc; i++)
        cout << "  " << expr[i];
    cout << endl; 
  */  

    bool or_flag = true;    // flag of OR
    /// while there are OR
    while(or_flag == true) {
        or_flag = false;
        /// for each component
        for(int i = 0; i < argc; i++) {
            /// if this is OR -> logic_operation()
            if(type[i]==OR) {
                or_flag = true;
                argc = logic_operation(i, argc, expr, type);
                expr[argc] = NULL;
            }
        }    
    }
    
    if(argc!=1) {
        cout << "  incorrect expression" << endl;
        return NULL;
    }
    

    /// return value of the expression
    return expr[0];
};


/// THE FUNCTION conducts operations '!' in point
//# RETURN new argc (the number of remaining arguments)
int Repl::negat_operation(int index, int argc, char** expr, int* type)
{
    char* val = new char [2];   // result of operation
    
    /// if was T -> F, else F -> T
    if(!strcmp(expr[index+1], "T")) {
        strcpy(val,"F");
        expr[index+1] = val;
    } else {
        strcpy(val,"T");
        expr[index+1] = val;
    }
   
    /// shift components in array, closing of waste
    for(int i = index+1; i < argc; i++) {
        expr[i-1] = expr[i];
        type[i-1] = type[i];
    } 
    
    /// the number of remaining arguments
    return argc-1;
};



int Repl::logic_operation(int index, int argc, char** expr, int* type)
{
    int shift = 2;  // shift of components in array
    char* res = new char [2];   // result of operation
    
    /// check correctness of operation (only logic around)
    if(type[index-1]!=LOG || type[index+1]!=LOG)
        return 0;
            
    /// choice type of operation and do it        
    switch(type[index])
    {
        case AND:
            /// if ( T && T ) -> T, else F
            if( !strcmp(expr[index-1],"T") && !strcmp(expr[index+1],"T")) {
                strcpy(res,"T");
                expr[index-1] = res;
            } else if( strcmp(expr[index-1],expr[index+1]) ||  (!strcmp(expr[index-1],"F") && (!strcmp(expr[index+1],"F"))) ) {
                strcpy(res,"F");
                expr[index-1] = res;
            }
            break;
        case OR:
            /// if ( F && F ) -> F, else T
            if( !strcmp(expr[index-1],"F") && !strcmp(expr[index+1],"F")) {
                strcpy(res,"F");
                expr[index-1] = res;
            } else if( strcmp(expr[index-1],expr[index+1]) ||  (!strcmp(expr[index-1],"T") && (!strcmp(expr[index+1],"T"))) ) {
                strcpy(res,"T");
                expr[index-1] = res;
            }
            break;
        default:
            return 0;
            break;
    }
    
    /// shift components in array, closing of waste
    for(int i = index+shift; i < argc; i++) {
        expr[i-shift] = expr[i];
        type[i-shift] = type[i];
    }
    
    /// return the number of remaining arguments
    return argc-shift;
};



int Repl::compar_operation(int i, int argc, char** expr, int* type)
{
    int shift = 2;  // shift of components in array
    char* res = new char [2];   // result of operation
    
    bool condition = false;
    /// choice type of operation and do it
    
    int op1 = 0;      
    int ten = strlen(expr[i-1]) - 1;
    for(int j = 0; j < strlen(expr[i-1]); j++)
        op1 = op1 + ((int)expr[i-1][j] - 48) * pow(10,ten--);
    int op2 = 0;      
    ten = strlen(expr[i+1]) - 1;
     for(int j = 0; j < strlen(expr[i+1]); j++)
        op2 = op2 + ((int)expr[i+1][j] - 48) * pow(10,ten--);
    

    switch(type[i])
    {
        case MORE:
            if(op1 > op2)
                condition = true;
            break;
        case LESS:
            if(op1 < op2)
                condition = true;
            break;;
        case MORE_EQ:
            if(op1 >= op2)
                condition = true;
            break;
        case LESS_EQ:
            if(op1 <= op2)
                condition = true;
            break;
        case EQUAL:
            if(op1 == op2)
                condition = true;
            break;
        case NOT_EQUAL:
            if(op1 != op2)
                condition = true;
            break;
        default:
            return 0;
            break;
    }
    
    
    if(condition == true) {
        strcpy(expr[i-1], "T");
        type[i-1] = LOG;
    } else {
        strcpy(expr[i-1], "F");
        type[i-1] = LOG;
    }
    
    
    /// shift components in array, closing of waste
    for(int j = i+shift; j < argc; j++) {
        expr[j-shift] = expr[j];
        type[j-shift] = type[j];
    }
    
    
    /// return the number of remaining arguments
    return argc-shift;        
    
};

/// THE FUNCTION parse the expression into argument type
//# RETURN argc - number of components of the expression
int Repl::logic_parser(int argc, char** argv)
{
    /// expression - the expression as char*
    char* expression = new char [BUFFER_SIZE];
    strcpy(expression, argv[1]);
    
    int index = 0;
    ///for each symbol of expression
    for(int i = 0; i < strlen(expression); i++) {   
        /// check '+', '-', '*', '\', '(', ')'
        if(expression[i] == '+' || expression[i] == '-' || expression[i] == '*' || expression[i] == '\\' || expression[i] == '(' || expression[i] == ')') { 
            char* op = new char [2];
            op[0] = expression[i];
            op[1] = '\0';
            argv[index++] = op;
            continue;
        } 
 
        /// check '<', '>', '<=', '>=', '!', '='
        if(expression[i] == '<' || expression[i] == '>' || expression[i] == '!') {
            if(expression[i+1] == '=') {
                char* op = new char [3];
                op[0] = expression[i];
                op[1] = '=';
                op[2] = '\0';
                argv[index++] = op;
                i++;
            } else {
                char* op = new char [2];
                op[0] = expression[i];
                op[1] = '\0';
                argv[index++] = op;
            }
            continue;
        }
               
        /// check '==', '&&', '||'
        if(expression[i] == '=' || expression[i] == '&' || expression[i] == '|') {
            if(expression[i+1] == expression[i]) {
                char* op = new char [3];
                op[0] = expression[i];
                op[1] = expression[i+1];
                op[2] = '\0';
                argv[index++] = op;
                i++;
                continue;
            }
        }
               
        /// if this symbol is NUM
        if((expression[i] >= '0' && expression[i] <= '9')) {
            char* op = new char [BUFFER_SIZE];
            int i_char = 0;
            while((expression[i]-48) >= 0 && (expression[i]-48) < 10){
                op[i_char] = expression[i++];
                i_char++;
            }
            op[i_char] = '\0';
            argv[index] = op;
            index++;
            i--;    
            continue;
        }
        
        /// if this symbol is T or F
        if(expression[i] == 'T' || expression[i] == 'F' ) {
            if( !((expression[i]>='A' && expression[i]<='Z') || (expression[i]>='a' && expression[i]<='z') \
            || (expression[i]>='0' && expression[i]<='9') || expression[i]=='_' ) )
            {
                char* op = new char [2];
                op[0] = expression[i];
                op[1] = '\0';
                argv[index++] = op;
                continue;
            }
        }
             
        /// if this symbol is VAR
        if( (expression[i]>='A' && expression[i]<='Z') || (expression[i]>='a' && expression[i]<='z') \
        || (expression[i]>='0' && expression[i]<='9') || expression[i]=='_' ) 
        {
            char* op = new char [MAX_VAR_LEN];
            int i_char = 0;
            while((expression[i]>='A' && expression[i]<='Z') || (expression[i]>='a' && expression[i]<='z') \
            || (expression[i]>='0' && expression[i]<='9') || expression[i]=='_' ) {
                op[i_char] = expression[i++];
                i_char++;
            }
            op[i_char] = '\0';
            argv[index] = op;
            index++;
            i--;    

            continue;      
        }
              
        /// if  this symbol is inaccessible symbol used
        cout << "  incorrect expression (inaccessible symbol used '" << expression[i] << "')" << endl; 
        return 0;
    }
    
    argv[index] = NULL;

    return index;
};


/// THE FUNCTION make type map of expression
//# RETURN TRUE - correct; FALSE - incorrect
bool Repl::logic_map(int argc, char** argv, int* type)
{
    ///for each component of expression
    for(int i = 0; i < argc; i++) {  
        if(!strcmp(argv[i],"+") || !strcmp(argv[i],"-") || !strcmp(argv[i],"*") || !strcmp(argv[i],"\\")) {
            type[i] = SIGN;
            continue;
        }
        /// if this is opened parenthess
        if(!strcmp(argv[i],"(")) {
            type[i] = P_OP;
            continue;
        }
        /// if this is closed parenthess
        if(!strcmp(argv[i],")")) {
            type[i] = P_CL;
            continue;
        }
        /// if this is equal
        if(!strcmp(argv[i],"==")) {
            type[i] = EQUAL;
            continue;
        }
        /// if this is not-equal
        if(!strcmp(argv[i],"!=")) {
            type[i] = NOT_EQUAL;
            continue;
        }
        /// if this is more
        if(!strcmp(argv[i],">")) {
            type[i] = MORE;
            continue;
        }
        /// if this is less
        if(!strcmp(argv[i],"<")) {
            type[i] = LESS;
            continue;
        }
        /// if this is more equal
        if(!strcmp(argv[i],">=")) {
            type[i] = MORE_EQ;
            continue;
        }
        /// if this is less equal
        if(!strcmp(argv[i],"<=")) {
            type[i] = LESS_EQ;
            continue;
        }
        /// if this is and
        if(!strcmp(argv[i],"&&")) {
            type[i] = AND;
            continue;
        }
        /// if this is or
        if(!strcmp(argv[i],"||")) {
            type[i] = OR;
            continue;
        }
        /// if this is not
        if(!strcmp(argv[i],"!")) {
            type[i] = NOT;
            continue;
        }
        /// if this is 
        if(!strcmp(argv[i],"T") || !strcmp(argv[i],"F")) {
            type[i] = LOG;
            continue;
        }
        /// if this is a number
        if( argv[i][0]>='0' && argv[i][0]<='9' ) {
            type[i] = NUM;
            continue;   
        }
        /// if this is a variable
        if( (argv[i][0]>='A' && argv[i][0]<='Z') || (argv[i][0]>='a' && argv[i][0]<='z') \
        || (argv[i][0]>='0' && argv[i][0]<='9') || argv[i][0]=='_' ) {
            type[i] = VAR;    
            continue;
        }
        
        /// if using of invalid characters
        cout << "  incorrect expression (using of invalid characters)" << endl;
        return false;
    }  
    
    return true;
};


/// THE FUNCTION check the expression on correctness
//# RETURN TRUE - correct; FALSE - incorrect
bool Repl::logic_syntax(int argc, char** argv, int* type)
{
    /// checking an equal number of opening and closing parentheses
    int n_open = 0;
    int n_close = 0;        
    for(int i =0; i < argc; i++) {
        if(type[i] == P_OP)
            n_open++;
        if(type[i] == P_CL)
            n_close++;
    }
    if(n_open > n_close) {
        cout << "  incorrect expression (missing closed parenthesis [" << (n_open-n_close) << "])" << endl;
        return false;
    }
    if(n_close > n_open) {
        cout << "  incorrect expression (missing opened parenthesis [" << (n_close-n_open) << "])" << endl;
        return false;
    }
    
    
    /// for each component
    for(int i = 0; i < (argc-1); i++) {
        /// checking operators of logic are near
        for(int j = EQUAL; j <= OR; j++)
            for(int k = EQUAL; k <= OR; k++)
                if(type[i]==j && type[i+1]==k) {
                    cout << "  incorrect expression (operators are near '" << argv[i] << argv[i+1] << "')" << endl;
                    return false;
                }
    
        /// checking operators of logic and math are near
        if(type[i]==SIGN)
            for(int j = EQUAL; j <= OR; j++)
                if(type[i+1]==j) {
                    cout << "  incorrect expression (operators are near '" << argv[i] << argv[i+1] << "')" << endl;
                    return false;    
                }
         
        /// checking operators of math and logic are near
        for(int j = EQUAL; j <= OR; j++)
            if(type[i]==j)
                if(type[i+1]==SIGN) {
                    cout << "  incorrect expression (operators are near '" << argv[i] << argv[i+1] << "')" << endl;
                    return false;   
                }
        
        /// checking operators of math are near
        if(type[i]==SIGN && type[i+1]==SIGN) {
            cout << "  incorrect expression (operators are near '" << argv[i] << argv[i+1] << "')" << endl;
            return false; 
        }
        
        /// checking NUM, VAR, LOG are near NUM, VAR and LOG
        if(type[i]==NUM && (type[i+1]==NUM || type[i+1]==VAR || type[i+1]==LOG)) {
            cout << "  incorrect expression (operands are near '" << argv[i] << argv[i+1] << "')" << endl;
            return false; 
        }
        if(type[i]==VAR && (type[i+1]==NUM || type[i+1]==VAR || type[i+1]==LOG)) {
            cout << "  incorrect expression (operands are near '" << argv[i] << argv[i+1] << "')" << endl;
            return false; 
        }
        if(type[i]==LOG && (type[i+1]==NUM || type[i+1]==VAR || type[i+1]==LOG)) {
            cout << "  incorrect expression (operands are near '" << argv[i] << argv[i+1] << "')" << endl;
            return false; 
        }    
        
        /// checking opened parenthess before operator
        if(type[i]==P_OP) {
            for(int j = EQUAL; j <= OR; j++)
                if(type[i+1]==j) {
                    cout << "  incorrect expression (opened parenthess before logical operator '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
                }
            if(type[i+1]==SIGN) {
                    cout << "  incorrect expression (opened parenthess before math operator '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
            }
        }
        
        /// checking closed parenthess after operator
        if(type[i+1]==P_CL) {
            for(int j = EQUAL; j <= OR; j++)
                if(type[i]==j) {
                    cout << "  incorrect expression (closed parenthess after logical operator '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
                }
            if(type[i]==SIGN) {
                    cout << "  incorrect expression (closed parenthess after math operator '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
            }
        }
        
        
        /// cheging closed parenthess before variable, number, logic symbol
        if(type[i]==P_CL) {
            if(type[i+1]==VAR) {
                    cout << "  incorrect expression (closed parenthess before variable '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
            }
            if(type[i+1]==NUM) {
                    cout << "  incorrect expression (closed parenthess before number '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
            }
            if(type[i+1]==LOG) {
                    cout << "  incorrect expression (closed parenthess before logic simbol '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
            }
        }
        
        /// cheging opened parenthess after variable, number, logic symbol
        if(type[i+1]==P_OP) {
            if(type[i]==VAR) {
                    cout << "  incorrect expression (opened parenthess after variable '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
            }
            if(type[i]==NUM) {
                    cout << "  incorrect expression (opened parenthess after number '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
            }
            if(type[i]==LOG) {
                    cout << "  incorrect expression (opened parenthess after logic simbol '" << argv[i] << argv[i+1] << "')" << endl;
                    return false; 
            }
        }  
        
        ///checking regulations of '!'
        if(type[i]==NOT && !(type[i+1]==P_OP || type[i+1]==LOG || type[i+1]==NOT)) {
            cout << "  incorrect expression (negation aren't near logic '" << argv[i] << argv[i+1] << "')" << endl;
            return false; 
        }
        if(type[i+1]==NOT && (type[i]==P_CL || type[i]==VAR || type[i]==NUM || type[i]==LOG)) {
            cout << "  incorrect expression (negation aren't near logic '" << argv[i] << argv[i+1] << "')" << endl;
            return false; 
        }
        
        /// checking opened and closed parenthesses are near
        if( (type[i]==P_OP && type[i+1]==P_CL) || (type[i]==P_CL && type[i+1]==P_OP) ) {
            cout << "  incorrect expression (opened and closed parenthesses are near '" << argv[i] << argv[i+1] << "')" << endl;
            return false; 
        }
        
        
        /// checking regulations of '&&' and '||'
        if( (type[i]==AND || type[i]==OR) && !(type[i+1]==P_OP || type[i+1]==LOG) ) {
            cout << "  incorrect expression (not the logical value near && '" << argv[i] << argv[i+1] << "')" << endl;
            return false; 
        }
        if( (type[i+1]==AND || type[i+1]==OR) && !(type[i]==P_CL || type[i]==LOG) ) {
            cout << "  incorrect expression (not the logical value near && '" << argv[i] << argv[i+1] << "')" << endl;
            return false; 
        }
                             
    }
    
    /// syntax of logic expression is correct
    return true;
};