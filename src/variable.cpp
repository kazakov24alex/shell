#include "inc/repl.h"

#include <cstdio>
#include <iostream> 
#include <unistd.h> 
#include <cstring>
#include <stdlib.h>
#include <fstream>
using namespace std; 


#define MAX_PATH 1024       // max path of files
#define MAX_VAR_NAME 256    // max lentgh of variable name
#define MAX_LINE_LEN 256    // max length of line from files

#define VAR_LIST "/.shell_variables"    // name of the file with variable list
#define EOL '\n'            // symbol of 'end of line'


/// THE FUNCTION assigns a value of expression (argv[2]) 
//# to the variable (argv[0]) 
//# RETURNS nothing   
void Repl::var_assign(int argc, char** argv)
{
    /// 'name' - name of the variable
    char* result = new char [MAX_LINE_LEN];
    char* name = new char [MAX_VAR_NAME];
    strcpy(name, argv[0]);
    
    /// check correctness of varibale name
    for(int i = 0; i < strlen(argv[0]); i++) {
        if( !((argv[0][i]>='A' && argv[0][i]<='Z') || (argv[0][i]>='a' && argv[0][i]<='z') \
        || (argv[0][i]>='0' && argv[0][i]<='9') || argv[0][i]=='_' ) )
        {
            cout << "  syntax error (invalid characters in a variable name)" << endl;
            return;
        }
    }
    
    /// identify type of variable: int or stirng 
    if(argv[2][0] == '\"') {
        int i = 1;
        while(argv[2][i] != '\"') {
            result[i-1] = argv[2][i];
            i++;
        }
        result[i-1] = '\0';        
    } 
    else {
        /// parse argv[2] into mathematical component of expression
        //# argc - number of mathematical components
        //# argc==0 (expression is incorrect) - return NULL
        argc = math_parser(argc, argv);
        if(argc == 0) {
            cout << "  incorrect expression" << endl;
            return;
        }
        
        /// calculate expression from (argv[2]) to 'result'
        int result_int = math_calculator(argc, argv);
        /// check completeness of calculations
        //# if calculations is complete - assign the value to the variable
        if(argv[0]==NULL) {
            cout << "  incorrect expression" << endl;
            return;
        }
        
        result = math_tostring(result_int);
    }
        
        
    
    /// print the result of asignment
    cout << "  " << name << " = " << result << endl; 
         
    ///if there is the variable now - delete it    
    var_delete(name);
        
    /// add the variables and its value to list of variables 
    char* var_file = new char [MAX_PATH];
    const char *home = getenv("HOME");
	strcpy(var_file, home);
	strcat(var_file, VAR_LIST);
    ofstream file_upload(var_file, ios_base::app);
    if(file_upload.is_open()) {
        strcat(name, " ");
        file_upload << name;
        file_upload << result;
        file_upload << EOL; 
    }
    file_upload.close();
};


/// THE FUNCTION receive a value of the variable
//# RETURNS a value of the variable or NULL (if vaiable douesn't exist)
char* Repl::var_value(char* name)
{
    /// var_file - path to file with list of variables
    char* var_file = new char [MAX_PATH];
    const char *home = getenv("HOME");
	strcpy(var_file, home);
	strcat(var_file, VAR_LIST);
    
    /// open file with list of variables
    ifstream file_read(var_file);
    if(file_read.is_open()) {
        char *line = new char [MAX_LINE_LEN];
        while (!file_read.eof()) {
            file_read.getline(line, MAX_LINE_LEN, '\n');
            
            /// put '\0' after name of variable
            int pos = 0;
            while(line[pos] != ' ' && pos < strlen(line))
                pos++;
            line[pos] = '\0';
            /// compare name of required and current variables
            //# return the remainder of the line, if the name is the same
            if(!strcmp(name, line)) {
                file_read.close();
                return (line+strlen(name)+1);
            }
        }    
    }
    file_read.close();
    
    /// return NULL, if variable was not found
    return NULL; 
};


/// THE FUNCTION deletes the vaiables from list of variables
//# RETURNS TRUE (the variable was deleted) or FALSE (the 
//# variables doesn't exist in the list of variables)
bool Repl::var_delete(char* name)
{
    /// try to get a value of the variable
    //# if it fails - return NULL
    if(var_value(name) == NULL)
        return false;
        
    /// combine source_line (var_val)- line to be deleted
    char* source_line = new char [MAX_LINE_LEN];
    strcpy(source_line, name);
    strcat(source_line, " ");
    strcat(source_line, var_value(name));

    /// var_file - path to file with list of variables
    char* var_file = new char [MAX_PATH];
    const char *home = getenv("HOME");
	strcpy(var_file, home);
	strcat(var_file, VAR_LIST);
    
    
    int lines_num = 0;  // counter of the remaining lines in the file
    char *line = new char [MAX_LINE_LEN];
    /// read the file to count lines
    ifstream file_count(var_file);
    if(file_count.is_open()) {
        while(!file_count.eof()) {
            file_count.getline(line, MAX_LINE_LEN);
            lines_num++;
        }
    }
    file_count.close();
    
     
    int i = 0;  // index of array of variables
    char* var_list[lines_num+1]; // array of variable's lines
    /// open file to read - read and save to array all line except empty or removed
    ifstream file_read(var_file);
    if(file_read.is_open()) {
        while(!file_read.eof()) {
            char* cmd = new char [MAX_LINE_LEN];
            file_read.getline(cmd, MAX_LINE_LEN);
            if(strcmp(cmd, source_line) && strlen(cmd) != 0)
                var_list[i++] = cmd;  
        }
    }
    var_list[i] = NULL;
    file_read.close();
    
    
    /// clear the file of variables
    ofstream file_clear(var_file, ios_base::trunc);
    file_clear.close();
    
    
    /// loading stored in an array lines in the variable's file
    ofstream file_upload(var_file, ios_base::app);
    char *com = new char [MAX_LINE_LEN];
    i = 0;
    while(var_list[i] != NULL) {
        strcpy(com, var_list[i++]);
        file_upload << com;
        file_upload << EOL;
    } 
    delete com;
    file_upload.close();
    
    /// removing variable was successful - return TRUE
    return true;   
};


/// THE FUNCTION shows a value of the variables
//# RETURNS nothing
void Repl::var_show(int argc, char** argv)
{
    /// check the number of arguments
    //# if argc==1 - print all vaiables from file
    if (argc == 1) {
        /// var_file - path to file with list of variables
        char* var_file = new char [MAX_PATH];
        const char *home = getenv("HOME");
	    strcpy(var_file, home);
	    strcat(var_file, VAR_LIST);
        
        char *line = new char [MAX_LINE_LEN];
        /// open file and read every line
        ifstream file_read(var_file);
        if(file_read.is_open()) {
            while(!file_read.eof()) {
                file_read.getline(line, MAX_LINE_LEN);
                /// if line isn't empty - print it
                if(strlen(line) != 0)
                    cout << "  " << line << endl;
            }
        }
        file_read.close();
        /// show is finished - quit the function
        return;
    }
    
    //# if argc!=1 - print only variables from arguments
    for(int i = 1; i < argc; i++) { 
        /// get a value of the variable
        char* value = var_value(argv[i]);
        /// if value was taken - combine line to print
        if(var_value(argv[i]) != NULL)
            cout << "  " << argv[i] << " = " << value << endl;
        /// if the variable  doesn't exist - pring message
        else
            cout << "  " << argv[i] << " does not exist" << endl;
    }
}; 


/// THE FUNCTION sends the variables to delete
//# RETURN nothing
void Repl::var_erase(int argc, char** argv)
{
    /// if line is "--" - print message and return
    if (argc == 1) {
        char* var_file = new char [MAX_PATH];
        const char *home = getenv("HOME");
	    strcpy(var_file, home);
	    strcat(var_file, VAR_LIST);
        ofstream file_clear(var_file, ios_base::trunc);
        file_clear.close();
        return;
    }
    
    /// for every argument - try to delete it
    for(int i = 1; i < argc; i++) { 
        /// if deleting of variables was successful - print message
        if(var_delete(argv[i]) == true)
            cout << "  " <<  argv[i] << " was deleted " << endl;
        /// if deleting of variables wasn't successful - print message
        else
            cout << "  " << argv[i] << " does not exist" << endl;
    }  
}


/// THE FUNCTION implement substitution of value instead variables
//# RETURN TRUE - if substitution is successfully completed
//# RETURN FALSE - if there are non-existent variables
bool Repl::var_substitution(int argc, char** argv)
{    
    /// for each component
    for(int i = 1; i < argc; i++) {   
        /// take the value of the variable
        //# if variable is non-exist - message + return FALSE
        if(var_value(argv[i]) != NULL)
            strcpy(argv[i], var_value(argv[i]));
    }
    
    /// if all variables are substituted - return TRUE
    return true;
}