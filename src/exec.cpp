#include "inc/repl.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdarg.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>


/// The function run the file entered as an argument
void Repl::exec_cmd(int argc, char** argv) {
	/// create a new process
    pid_t p;
	p = 0;
	p = fork();
	int* status;
	wait(status);
    
    /// run executive file
	if (p == 0) {
		if (execvp(argv[0], argv) == -1)
            cout << "  file '" << argv[0] << "' is not executable" << endl; 
		exit(-1);
	}
}