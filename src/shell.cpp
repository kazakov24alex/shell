#include "inc/term.h"
#include "inc/repl.h"

#include <cstdio>
#include <iostream>
#include <string>
using namespace std;

#define MAX_STR_LEN 4096

/// prototypes of functions
int arg_parse(string arg);
void help();
void version();
void read(char* file_path);

/// global variables
char arg_lett[] = {'h', 'v'};   // single keys of functions
string arg_word[] = {"--help", "--version"};    // full keys of functions
enum arg_name {HELP, VERSION};  // enumeration of functions
void (*arg_func[sizeof(arg_word)])() = {help, version}; // functions array
bool arg_enabled[sizeof(arg_word)] = {false};   // flags array of switched on functions 


int main(int argc, char *argv[]) 
{
    /// count the number of arguments and 
    //# determine the appropriate response to them    
    if(argc > 1)
    {
        for(int n_arg = 1; n_arg < argc; n_arg++) {
            arg_parse(argv[n_arg]);
        }        
               
        for(int i = 0; i < sizeof(arg_word); i++) {
            if(arg_enabled[i] == true) {
                (*arg_func[i])();
            }
        }
        /// shut down the programe, if an argument is invoked
        return 1;
    }
    
	term_set_driver();     // turn off the echo type of input
	Repl repl;             // Shell starts
	term_reset_driver();   // turn on the echo type of input

	return 0;
}


/// The function parse arguments of the programe
int arg_parse(string arg)
{
    if(arg[0] == '-' && arg[1] == '-') {
        for(int i = 0; i < sizeof(arg_lett); i++) {
            if(arg == arg_word[i])
            {
                arg_enabled[i] = true;
                break;
            }
            if(i == (sizeof(arg_lett)-1))
                cout << "Argument '" << arg << "' not found" << endl; 
        }        
    }
    else if(arg[0] == '-' && arg[1] != '-') {
        for(int n_lett = 1; n_lett < arg.length(); n_lett++) {
            for(int i = 0; i < sizeof(arg_lett); i++) {
                if(arg[n_lett] == arg_lett[i])
                {
                    arg_enabled[i] = true;
                    break;
                }
                if(i == (sizeof(arg_lett)-1))
                    cout << "Argument '-" << arg[n_lett] << "' not found" << endl; 
            }
        }
    }    
    else
        cout << "Argument '" << arg << "' not found" << endl;        
}


/// The function transmits path to the help file
void help()
{
    char path[] = "/usr/share/shell/help";
    read(path);   
}


/// The function transmits path to the version file
void version()
{
    char path[] = "/usr/share/shell/version";
    read(path);   
}


/// The function reads shared file and displays its contents
void read(char* file_path)
{
    /// open file and read it, writting its line on screen
    char *str = new char [MAX_STR_LEN];
    ifstream f(file_path);
    if (f.is_open()) {
        while (!f.eof()) {
            f.getline(str, MAX_STR_LEN, '\n');
            cout << "  " << str << endl;
        }
    }
    else
        cout << "  Sorry, the information is unavailable now." << endl;
    
    /// close file
    f.close();
}