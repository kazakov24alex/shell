#include "inc/repl.h"

#include <cstdio>
#include <iostream>
#include <unistd.h>
#include <cstring>
using namespace std;

#define MAX_CWD 256
#define MAX_CMD_LEN 10
#define MAX_CMD_NUM 256


void Repl::print_cwd()
{
    char cwd[MAX_CWD];
    getcwd(cwd, MAX_CWD);
    strcat(cwd,"/");
    char invite[] = " > ";
     
    write(STDOUT_FILENO, cwd, strlen(cwd));
    write(STDOUT_FILENO, invite, strlen(invite));
};


void Repl::add_char()
{
    int nbs;    // the number of bytes per symbol
    /// find the number of bytes in a character
    if (c[0] == -48 || c[0] == -47)
        nbs = 2;
    else
        nbs = 1;
    len += nbs;
    
    /// pushing the chars to the right from the current 
    //# position in the buffer for the insertion of received symbol
    for (char* i = buf + len ; i > cur ; i--) {
	   *i = *(i-nbs);
    }
    *(cur++) = c[0];
    if(nbs == 2) {
    	read(STDIN_FILENO, c, 1);
        *(cur++) = c[0];
    }    
 
    /// write characters after the added symbol
	for (char* i = cur-nbs; i < buf + len ; i++) {
	    write(STDOUT_FILENO, i, 1);
	}

    /// delete characters after the added symbol, written earlier
    //# take into account the possibility of double-byte characters
    for (char* i = cur ; i < buf + len ; i++) {
        if(*i == -48 || *i == -47)
	       i++;
        write(STDOUT_FILENO, "\b", 1);   
	}   
};


void Repl::cursor_left()
{
    /// if the cursor is not in the leftmost position, then move it to the left by one character
    //# take into account the possibility of double-byte characters
    if (cur > buf) {
        if (*(cur-2) == -48 || *(cur-2) == -47)
            cur--;
        write(STDOUT_FILENO, "\b", 1);
	    cur--;
	}
};

void Repl::cursor_right()
{
    /// if the cursor is not in the rightmost position, then move it to the right by one character
    //# take into account the possibility of double-byte characters
    if (cur < buf + len) {
        if (*(cur) == -48 || *(cur) == -47)
            write(STDOUT_FILENO, cur++, 1);
        write(STDOUT_FILENO, cur++, 1); 
	}    
};


void Repl::erase_char()
{
    /// delete the last symbol from input-line and remove it from buffer
	write(STDOUT_FILENO, "\b \b", 3);
	remove_char_from_buffer(); 
};


void Repl::remove_char_from_buffer() {
    /// find the number of bytes in erasing character 
    // correct the the new length and current position 
    int nbs;    // the number of bytes per symbol 
    if (*(cur-2) == -48 || *(cur-2) == -47)
        nbs = 2;
    else
        nbs = 1;   
    cur -= nbs;
    len -= nbs;    
        
    /// move the characters to the right of the deleted character 
    //# position by one discharhe to the left 
    /// re-derive the sequence of characters in the input line
    for (char* i = cur ; i <= buf+len ; i++) {
	   *i = *(i+nbs);
       write(STDOUT_FILENO, i, 1);
    }
    write(STDOUT_FILENO, " ", 1);
    
    /// remove the old copy of the sequence of characters from the input line
    for (char* i = cur ; i <= buf+len ; i++) {
		if(*i == -48 || *i == -47)
	       i++;
        write(STDOUT_FILENO, "\b", 1);   
	}
};


int Repl::parser_arg(char* buff, char** argv) 
{
	int argc = 0;
    
	while (1) {
		if (*buff == '\0')
			break;
        
        argv[argc++] = buff;
        
		/// Search begin of next argument
		short notEscaped = 1;
		short slashEscaped = 0;

		while (*buff != '\0') {
			if (*buff == ' ' && notEscaped)
				break;
			if (*buff == '"' && !slashEscaped)
				notEscaped = !notEscaped;

			slashEscaped = 0;
			if (*buff == '\\')
				slashEscaped = 1;
            
            buff++;
		}

		if (*buff != '\0') {
			*buff = '\0';
			buff++;
		} else {
			break;
		}
	}
	argv[argc] = NULL;

	return argc;
};


void Repl::replace_buf(char* new_buf) {
	while (cur > buf) {
		erase_char();
	}
    
    
	while ( ((*new_buf) != '\n') && ((*new_buf) != '\0') ) {
		*(cur++) = *(new_buf++);
	}

	*(cur) = '\0';
    len = strlen(buf);
	write(STDOUT_FILENO, buf, strlen(buf));
}