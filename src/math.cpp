#include "inc/repl.h"

#include <cstdio>
#include <iostream> 
#include <unistd.h> 
#include <cstring>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <cmath>
#include <fstream>
using namespace std; 


#define BUFFER_SIZE 256     // the number of arguments in the line

#define MAX_PATH 1024
#define MAX_LINE_LEN 256
#define MAX_VAR_NAME 256
#define MAX_VAR_LEN 9

#define EOL '\n'// symbol of end of line

#define SUM 43  // ASCII code of '+'
#define SUB 45  // ASCII code of '-'
#define MUL 42  // ASCII code of '*'
#define DIV 92  // ASCII code of '\'
#define BEG 40  // ASCII code of '('
#define END 41  // ASCII code of ')'

#define SIGN 1  // type of signs
#define NUM 2   // type of number
#define VAR 3   // type of variable
#define P_OP 4  // type of opening parenthesis
#define P_CL 5  // type of closing parenthesis


/// THE FUNCTION calculate a value of the expression
//# RETURN a value of the expression in char* form
//# or NULL - if the expression incorrent
int Repl::math_calculator(int argc, char** argv)
{           
    int type[argc]; //= new int [argc]; // map of tupes of math components
;
    /// check the syntax of the mathematical expression
    //# if expression is incorrect - return NULL
    if(math_syntax(argc, argv, type) != true) {
        argv[0] = NULL;
        return 0;
    }

    /// update the number of components of the expression
    argc = 0;
    while(argv[argc] != NULL)
        argc++;
     
       
    /// substitute variables in the expression
    //# if installation is not completed correctly - return NULL
    if(math_substitution(argc, argv, type) != true) {  
        argv[0] = NULL;
        return 0;
    }

    int expr[argc];   // array of integer representation of the expression 
    /// transorm string form of number in the expression into integral form
    math_transformation(argc, argv, expr, type);
 
    /// calculate a value of the expression;
    int res = math_calculation(argc, expr, type);
                   
    return res;
};


/// THE FUNCTION implement substitution of number value instead variables
//# RETURN TRUE - if substitution is successfully completed
//# RETURN FALSE - if there are non-existent variables
bool Repl::math_substitution(int argc, char** argv, int* type)
{
    /// for each component
    for(int i = 0; i < argc; i++) {
        /// skip signs, numbers and parentheses
        if(!(type[i]==VAR))
            continue;
                
        /// take the value of the variable
        //# if variable is non-exist - message + return FALSE
        if(var_value(argv[i]) == NULL) {
            cout << "  incorrect expression (non-exist variable '" << argv[i] << "')" << endl; 
            return false;
        }
        /// substitute its value instead the variable namespace 
        else
            strcpy(argv[i], var_value(argv[i]));
    
        /// check type of variable
        if( !(argv[i][0]>='0' && argv[i][0]<='9') ) {
             cout << "  incorrect expression (string variable in the mathematical expression)" << endl;
             return false;
        }
    }    
    
    /// if all variables are substituted - return TRUE
    return true;
}


/// THE FUNCTION transforms number value from char* form to int
//# RETURN nothing
void Repl::math_transformation(int argc, char** argv, int* expr, int* type)
{
    /// for each component
    for(int i = 0; i < argc; i++) {
        /// signs, numbers and parentheses transform into their ASCII code
        if(type[i]==SIGN || type[i]==P_OP || type[i]==P_CL) {
            expr[i] = (int)(argv[i][0]);
            continue;
        }
        
        /// transform a number        
        expr[i] = 0;      
        int ten = strlen(argv[i]) - 1;
        for(int j = 0; j < strlen(argv[i]); j++)
            expr[i] = expr[i] + ((int)argv[i][j] - 48) * pow(10,ten--);
    }
}


/// THE FUNCTION calculate a value of the exression
//# RETURN a value of the expression in int form
int Repl::math_calculation(int argc, int* expr, int* type)
{          
    bool parentheses = true;    // flag of parentheses
    /// while there are parentheses
    while(parentheses == true) {
        parentheses = false;
        /// for each component
        for(int i = 0; i < argc; i++) {
            /// if found opening parenthess, then we look for the closing
            //# parenthess and send the contents of the parentheses in the
            //# math_calculation()     
            if(type[i] == P_OP) {
                parentheses = true;
                int j = i + 1;
                int opened = 0; 
                while(j < argc) {
                    if (type[j] == P_OP)
                        opened++;
                    if (type[j] == P_CL)
                        if (opened == 0)
                            break;
                        else
                            opened--;
                    j++;
                }
                
                int subexpr[j-i-1];
                int subtype[j-i-1];
                int subargc = 0;
                for(int k = i+1; k < j; k++) {
                    subexpr[subargc] = expr[k];
                    subtype[subargc] = type[k];
                    subargc++;
                }
                                    
                math_calculation(subargc, subexpr, subtype);
                expr[i] = subexpr[0];
                type[i] = NUM;

                for(int k = j+1; k < argc; k++) { 
                    expr[k-(j-i)] = expr[k];
                    type[k-(j-i)] = type[k];
                }
                argc = argc - (j-i);  
            }
        }    
    }   
 
    
    bool mul_div = true;    // flag of MUL and DIV
    /// while there are MUL or DIV
    while(mul_div == true) {
        mul_div = false;
        /// for each component
        for(int i = 0; i < argc; i++) {
            /// if this is MUL or DIV -> math_operation()
            if((expr[i] == MUL || expr[i] == DIV) && type[i]==SIGN) {
                mul_div = true;
                argc = math_operation(i, argc, expr, type);
            }
        }    
    }   

    bool sum_sub = true;    // flag of SUM and SUB
    /// while there are SUM or SUB
    while(sum_sub == true) {
        sum_sub = false;
        /// for each component
        for(int i = 0; i < argc; i++) {
            /// if this is SUM or SUB -> math_operation()
            if((expr[i] == SUM || expr[i] == SUB) && type[i]==SIGN) {
                sum_sub = true;
                argc = math_operation(i, argc, expr, type);
            }
        }    
    }   
    
    /// return a value of the expression              
    return expr[0];
};


/// THE FUNCTION conducts operations in point
//# RETURN new argc (the number of remaining arguments)
int Repl::math_operation(int index, int argc, int* expr, int* type)
{    
    int shift = 2;  // shift of components in array
    
    /// choice type of operation and do it
    switch(expr[index])
    {
        case SUM:
            expr[index-1] = expr[index-1] + expr[index+1];
            break;
        case SUB:
            expr[index-1] = expr[index-1] - expr[index+1];
            break;
        case MUL:
            expr[index-1] = expr[index-1] * expr[index+1];
            break;
        case DIV:
            expr[index-1] = expr[index-1] / expr[index+1];
            break;
        default:
            cout << "WHAT?!" << endl;
            break; 
    }
    
    /// shift components in array, closing of waste
    for(int i = index+shift; i < argc; i++) {
        expr[i-shift] = expr[i];
        type[i-shift] = type[i];
    }
    
    /// return the number of remaining arguments
    return argc-shift;
};
  

/// THE FUCTION transforms number from int form to char*
//# RETURN this number in char* form
char* Repl::math_tostring(int var)
{
    int index = 0;

    char* res = new char [9];


    if(var == 0) 
       return strcpy(res, "0");
    
    int len = 0;
    int numb = var;
    while(numb) {
        numb/=10;
        len++;
    }

    int ten = len - 1;
    
    if(var < 0) {
        res[index++] = '-';
        var = var * (-1);
        len++;
    }
    int num;
    for(index; index < len; index++) {
        num = var / pow(10, ten);
        res[index] = (char)(num + 48);
        var = var - num*pow(10, ten);
        ten--;
    }
    res[index] = '\0';

    return res;        
};


/// THE FUNCTION parse the expression into argument type
//# RETURN argc - number of components of the expression
int Repl::math_parser(int argc, char** argv)
{
    /// check correctness of input line
    if(argc < 3){
        cout << "  incorrect expression (too few arguments)" << endl;
        return 0;
    }
    if(argc > 3){
        cout << "  incorrect expression (too many arguments)" << endl;
        return 0;
    }
    
    /// expression - the expression as char*
    char* expression = new char [BUFFER_SIZE];
    strcpy(expression, argv[2]);

    int index = 0;
    /// for each symbol of expression
    for(int i = 0; i < strlen(expression); i++)
    {
        /// if this symbol is SIGN or parenthess
        if(expression[i] == '+' || expression[i] == '-' || expression[i] == '*' || expression[i] == '\\' || expression[i] == '(' || expression[i] == ')') { 
            char* op = new char [2];
            op[0] = expression[i];
            op[1] = '\0';
            argv[index] = op;
            index++;
            continue;
        }
        
        /// if this symbol is NUM
        if((expression[i] >= '0' && expression[i] <= '9')) {
            char* op = new char [BUFFER_SIZE];
            int i_char = 0;
            while((expression[i]-48) >= 0 && (expression[i]-48) < 10){
                op[i_char] = expression[i++];
                i_char++;
            }
            op[i_char] = '\0';
            argv[index] = op;
            index++;
            i--;    
            continue;
        }
        
        /// if this symbol is VAR
        if( (expression[i]>='A' && expression[i]<='Z') || (expression[i]>='a' && expression[i]<='z') \
        || (expression[i]>='0' && expression[i]<='9') || expression[i]=='_' ) 
        {
            char* op = new char [9];
            int i_char = 0;
            while((expression[i]>='A' && expression[i]<='Z') || (expression[i]>='a' && expression[i]<='z') \
            || (expression[i]>='0' && expression[i]<='9') || expression[i]=='_' ) {
                op[i_char] = expression[i++];
                i_char++;
            }
            op[i_char] = '\0';
            argv[index] = op;
            index++;
            i--;    

            continue;      
        }
        
        /// if  this symbol is inaccessible symbol used
        cout << "  incorrect expression (inaccessible symbol used '" << expression[i] << "')" << endl; 
        return 0;
    }
    
    argv[index] = NULL;

    return index;
};


/// THE FUNCTION check the expression on correctness, make type map of expression
//# RETURN TRUE - correct; FALSE - incorrect
bool Repl::math_syntax(int argc, char** argv, int* type)
{
    ///for each component of expression
    for(int i = 0; i < argc; i++) {
        /// if this is sign
        if(!strcmp(argv[i],"+") || !strcmp(argv[i],"-") || !strcmp(argv[i],"*") || !strcmp(argv[i],"\\")) {
            type[i] = SIGN;
            continue;
        }
        /// if this is opened parenthess
        if(!strcmp(argv[i],"(")) {
            type[i] = P_OP;
            continue;
        }
        /// if this is closed parenthess
        if(!strcmp(argv[i],")")) {
            type[i] = P_CL;
            continue;
        }
        /// if this is a number
        if( argv[i][0]>='0' && argv[i][0]<='9' ){
            type[i] = NUM;
            continue;   
        }
        /// if this is a variable
        if( (argv[i][0]>='A' && argv[i][0]<='Z') || (argv[i][0]>='a' && argv[i][0]<='z') \
        || (argv[i][0]>='0' && argv[i][0]<='9') || argv[i][0]=='_' ) {
            type[i] = VAR;    
            continue;
        }
        
        /// if using of invalid characters
        cout << "  incorrect expression (using of invalid characters)" << endl;
        return false;
    }    

    /// checking the minus in beginning of the expression
    if(!strcmp(argv[0],"-") && (type[1]==NUM || type[1]==VAR || type[1]==P_OP)) {
        for(int i = (argc-1); i >= 0; i--) {
            argv[i+1] = argv[i];
            type[i+1] = type[i];
        }
        char* op = new char [MAX_VAR_NAME];
        strcpy(op,"0");
        argv[0] = op;  
        type[0] = NUM;
        argc++; 
        argv[argc] = NULL;
    }

    /// checking the minus after opening parenthess
    for (int i = 0; i < (argc-1); i++) {
        if(type[i] == P_OP && !strcmp(argv[i+1],"-")) {
            for(int j = (argc-1); j >= i+1; j--) {
                argv[j+1] = argv[j];
                type[j+1] = type[j];
            }
            char* op = new char [MAX_VAR_NAME];
            strcpy(op,"0");
            argv[i+1] = op;  
            type[i+1] = NUM;
            argc++;
            argv[argc] = NULL;
        }
    }
    
    /// checking the existence of variables    
    for(int i = 0; i < argc; i++)
        if(type[i] == VAR)
            if(var_value(argv[i]) == NULL) {
                cout << "  incorrect expression (non-existent variable '" << argv[i] << "')" << endl;
                return false;
            }
    
    /// checking an equal number of opening and closing parentheses
    int n_open = 0;
    int n_close = 0;        
    for(int i =0; i < argc; i++) {
        if(type[i] == P_OP)
            n_open++;
        if(type[i] == P_CL)
            n_close++;
    }
    if(n_open > n_close) {
        cout << "  incorrect expression (missing closed parenthesis [" << (n_open-n_close) << "])" << endl;
        return false;
    }
    if(n_close > n_open) {
        cout << "  incorrect expression (missing opened parenthesis [" << (n_close-n_open) << "])" << endl;
        return false;
    }
    
    /// checking a sign in the beggining of the expression
    if(type[0] == SIGN) {
        cout << "  incorrect expression (missing operand in the beginning of the line)" << endl;
        return false;
    }
    
    /// for each components
    for(int i = 0; i < (argc-1); i++) {
        /// checking situation, when closed parenthess before number or variable
        if(type[i]==P_CL && (type[i+1]==NUM || type[i+1]==VAR )) {
            cout << "  incorrect expression (missed sign before '" << argv[i+1] << "')" << endl;
            return false; 
        }
        /// checking situation, when number or variable before opening parenthess
        if((type[i]==NUM || type[i]==VAR) && type[i+1] == P_OP ) {
            cout << "  incorrect expression (missed sign after '" << argv[i] << "')" << endl;
            return false; 
        }
        /// checking situation, when number or variable before number or variable
        if((type[i]==NUM || type[i]==VAR) && (type[i+1]==NUM || type[i+1]==VAR)) {
            cout << "  incorrect expression (operands '" << argv[i] << "' and '" << argv[i+1] << "' are not separated)" << endl;
            return false; 
        }
        /// checking situation, when sign before closed parenthess
        if(type[i]==SIGN && type[i+1]==P_CL) {
            cout << "  incorrect expression (missed operand after sign '" << argv[i] << "')" << endl;
            return false;
        } 
        /// checking situation, when closed parenthess before sign
        if(type[i]==P_OP && type[i+1]==SIGN) {
            cout << "  incorrect expression (missed operand after sign '" << argv[i] << "')" << endl;
            return false;
        }
        /// checking situation, when sign before sign
        if(type[i]==SIGN && type[i+1]==SIGN) {
            cout << "  incorrect expression (missed operand after sign '" << argv[i] << "')" << endl;
            return false;
        }
        /// checking situation, when closed parenthess before opened parenthess
        if(type[i]==P_CL && type[i+1]==P_OP) {
            cout << "  incorrect expression (missed sign after '" << argv[i] << "')" << endl;
            return false;
        }
        /// checking situation, when closed parenthess before opened parenthess
        if(type[i]==P_OP && type[i+1]==P_CL) {
            cout << "  incorrect expression (missed operand after '" << argv[i] << "')" << endl;
            return false;
        }
    }
    
    /// syntax of expression is correct!
    return true;
};