#include "inc/repl.h"

#include <cstdio>
#include <iostream> 
#include <unistd.h> 
#include <cstring>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <cmath>
#include <errno.h>

#define MAX_FILENAME 256
#define MAX_PATH 1024
#define MAX_LINE_LEN 1024
#define UID_GID_MAX 8
#define PASSWD "/etc/passwd"
#define GROUP "/etc/group"


/// The function changes the permissions of files
void Repl::chmod_cmd(int argc, char** argv)
{
    char cwd[MAX_FILENAME];     // current working directory
    getcwd(cwd, MAX_FILENAME); 
    strcat(cwd, "/");
    int mode[3];                // OCT permissions in string format
    
    /// translate permissions from the string to int
    for(int i = 0; i < 3; i++)
        mode[i] = (int)argv[1][i] - 48;
    
    /// check correctness of the command    
    if(argc < 3) {
        cout << "  you entered toocommand incorrectly" << endl;
        return;
    }
    else if(strlen(argv[1]) != 3) {
        cout << "  you incorrectly entered number of permisions" << endl;
        return;
    }
    else for(int i = 0; i < 3; i++) {
        if(mode[i] > 9 && mode[i] < 0) {
            cout << "  you incorrectly entered of permisions [" << (i+1) << "]" << endl;
            return;    
        } 
    }
    
    /// translate permissions from the OCT to DEC  
    int mode_dec =  mode[0] * (8*8) + mode[1] * (8) + mode[0] * (1);
    
    /// apply these permissions for all following arguments
    for(int i = 2; i < argc; i++) {
        char path[MAX_PATH];
        strcpy(path, cwd);
        strcat(path, argv[i]);
        
        /// show the notification of success or failure
        if(chmod(path, mode_dec) == 0)
            cout << "  file '" << argv[i] << "' received permissions '" << argv[1] << "'" << endl;
        else
            cout << "  can not access the file '" << argv[i] << "'; could not change the permissions" << endl;
    }
}


/// The function changes owner of files
void Repl::chown_cmd(int argc, char** argv)
{   
    char cwd[MAX_FILENAME];     // current working directory
    getcwd(cwd, MAX_FILENAME); 
    strcat(cwd, "/");
    
    /// check correctness of the command
    if(argc == 1) {
        cout << "  you entered too few arguments" << endl;
        return;
    } 
    
    /// extract UID of entered user
    int uid = uid_gid_extract(argv[1], true);  
    
    /// chack existance of entered user    
    if(uid == -1) {
        cout << "  this user does not exist in the system" << endl;
        return;
    }
    
    /// changes owner for each file from arguments
    for(int i = 2; i < argc; i++) {
        struct stat st;

        /// glue path
        char path[MAX_PATH];    // path of the file
        strcpy(path, cwd);
        strcat(path, argv[i]);
        
        /// get current GID
        if(lstat(path, &st)) {
			cout << "  file '" << argv[i] << "' not found" << endl;
            continue;
	    } 
        
        /// change owner of the file
        if(chown(path, uid, st.st_gid) == 0) 
            cout << "  owner of the file '" << argv[i] << "' is changed to '" << argv[1] << "'" << endl;
        else {
            cout << "  failed to change the user of file '" << argv[i] << "'" << endl;
            cout << "  make sure that you have enough permissions for this" << endl;
        }
    }  
}


/// The function changes owner of files
void Repl::chgrp_cmd(int argc, char** argv)
{   
    char cwd[MAX_FILENAME];     // current working directory
    getcwd(cwd, MAX_FILENAME); 
    strcat(cwd, "/");
    
    /// check correctness of the command
    if(argc == 1) {
        cout << "  you entered too few arguments" << endl;
        return;
    } 
    
    /// extract GID of entered user
    int gid = uid_gid_extract(argv[1], false);  

    /// chack existance of entered group
    if(gid == -1) {
        cout << "  this group does not exist in the system" << endl;
        return;
    }
    
    /// changes group for each file from arguments
    for(int i = 2; i < argc; i++) {
        struct stat st;

        /// glue path
        char path[MAX_PATH];    // path of the file
        strcpy(path, cwd);
        strcat(path, argv[i]);
        
        /// get current GID
        if(lstat(path, &st)) {
			cout << "  file '" << argv[i] << "' not found" << endl;
            continue;
	    } 
        
        /// change group of the file
        if(chown(path, st.st_uid, gid) == 0) 
            cout << "  group of the file '" << argv[i] << "' is changed to '" << argv[1] << "'" << endl;
        else {
            cout << "  failed to change the group of file '" << argv[i] << "'" << endl;
            cout << "  make sure that you have enough permissions for this" << endl;
        }
    }    
}


/// The function is extract UID and GID for nameuser from PASSWD or GROUP
int Repl::uid_gid_extract(char* name, bool users)
{
        int colon_pos[3];       // positions of colon in the PASSWD or GROUP line
        char uid[UID_GID_MAX];  // UID of file in the string format
        char gid[UID_GID_MAX];  // UID of file in the string format
        int len = strlen(name);
        char* line = new char [MAX_LINE_LEN];   // line from the PASSWD or GROUP

        
        /// open PASSWD or GROUP file to read
        char* source = new char [MAX_PATH];
        if (users == true)
            strcpy(source, PASSWD);
        else
            strcpy(source, GROUP);
            
        ifstream file(source);
        if(file.is_open()) {
            /// while not end of file = looking for entered user
            while (!file.eof()) {
                file.getline(line, MAX_LINE_LEN, '\n');
                
                /// if next symbol after username isn't ':' - next line
                if(line[len] != ':')
                    continue;        
                
                /// if next symbol after username isn't ':' - compare names
                //# else read next line
                line[len] = '\0';
                if(!strcmp(name, line) ) {
                    line[len] = ':';
                        
                    /// looking for position of the first 4 colons in the line
                    int pos = 0;
                    for(int i = 0; pos < 3; i++)
                    if(line[i] == ':')
                    colon_pos[pos++] = i;    
          
                    /// relatively colons extract UID from the line
                    pos = 0;
                    for(int i = colon_pos[1]+1; i < colon_pos[2]; i++)
                        uid[pos++] = line[i];
                    uid[pos] = '\0';
        
                    /// translate uid or gid from string form to int
                    int _uid = 0;      
                    int ten = strlen(uid) - 1;
                    for(int i = 0; i < strlen(uid); i++)
                        _uid = _uid + ((int)uid[i] - 48) * pow(10,ten--);

                    /// transfer the extracted UID or GID
                    file.close();
                    return _uid;  
                }       
            }
        }
        file.close();
        
        /// transfer incorrect UID and GID
        return -1;
}