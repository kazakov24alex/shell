#include "inc/repl.h"

#include <cstdio>
#include <iostream>
#include <cstring>
#include <fstream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
using namespace std;

#define MAX_FILENAME 256
#define MAX_LEN_PATH 1024
#define MAX_LINE_LEN 256
#define EOL '\n'
#define HOW_MUCH_TABS 3
#define UMASK 0777


/// The function sequentially outputs the specified files 
//# (or device), thereby combining them into a single stream
void Repl::cat_cmd(int argc, char** argv)
{
    int pos = 0;    // position of '>' in the arguments of the command
    char *line = new char [MAX_LINE_LEN];
    
    /// looking for the first mention of '>'
    for(pos = 0; pos < argc; pos++)
        if (strcmp(argv[pos],">") == 0)
            break;
    
    /// check on the correctness
    if( pos < (argc-2) || pos == (argc-1) ) {
        cout << "You entered incorrect arguments" << endl;    
        return;   
    }
    
    /// if '>' was present
    if(pos != argc) {
        /// write to output file
        ofstream f_input(argv[pos+1],ios_base::app);
        for(int i = 1; i < pos; i++) {
            ifstream f_read(argv[i]);
            if(!f_read.is_open())
                cout << "  can not open file'" << argv[i] << "'" << endl;
            else {
                while(!f_read.eof()) {
                    f_read.getline(line, MAX_LINE_LEN, '\n');
                    f_input << line;
                    f_input << EOL;
                } 
            }
            f_read.close();
        }
        f_input.close();
    }
    else {
        /// write to the terminal
         for(int i = 1; i < pos; i++) {
            ifstream f_read(argv[i]);
            if(!f_read.is_open())
                cout << "  can not open file'" << argv[i] << "'" << endl;
            else {
                while(!f_read.eof()) {
                    f_read.getline(line, MAX_LINE_LEN, '\n');
                    cout << "    " << line << endl;
                }
            } 
            f_read.close();
        }
    }
}


/// The function show attributes of the file
void Repl::file_cmd(int argc, char** argv)
{
    struct stat st;
    char cwd[MAX_FILENAME];
    
    getcwd(cwd, MAX_FILENAME);
    strcat(cwd, "/");
    
    for(int i = 1; i < argc; i++) {
            char full_name[MAX_LEN_PATH];
            strcpy(full_name, cwd);
			strcat(full_name, argv[i]);
			
            /// read attributes
			if(lstat(full_name, &st)){
                /// attributes not read - print error message
				cout << "  file '" << argv[i] << "' not found" << endl;
                return;
			} 
            /// attributes was read -  print the content with them
            else {
                /// print a name of the file
                cout << "\t" << argv[i];
                
                /// set tabulation (considering names of full two-bytes type)
                int tb = 1;  // the number of bytes in the first character
                if (argv[i][0] == -48 || argv[i][0] == -47)
                    tb = 2;
                int tab_len = (strlen(argv[i])/tb)/8;  // as much as tabs in a word
                for(int i = 0; i < (HOW_MUCH_TABS-tab_len); i++)
                    cout << "\t";
                
                /// print attributes of the content
                switch(st.st_mode & S_IFMT){
					case S_IFLNK:
                        cout << "(link)\t";
                        break;
					case S_IFDIR:
                        cout << "(directory)\t";
                        break;
					case S_IFREG:
						cout  << "(regular, " << st.st_size << " bytes)";
                        break;
					case S_IFCHR:
						cout  << "(device)\t";
                        break;
					case S_IFBLK:
                        cout << "(device)\t";
                        break;
					case S_IFIFO:
                        cout << "(FIFO)\t";
                        break;
					case S_IFSOCK:
                        cout << "(socket)\t";
                        break;
					default:
                        cout << "(undefined type)\t";
                        break;
				}
			}
            /// print UID and GID of the file
            cout << "\tUID=" << st.st_uid << "   \tGID=" << st.st_gid << endl;	
		}
}