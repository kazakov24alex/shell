#include "inc/repl.h"

#include <cstdio>
#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

#define BUFFER_SIZE 256
#define HISTORY_SIZE 500
#define MAX_FILENAME 256
#define MAX_LEN_PATH 1024
#define MAX_CMD_NUM 256

#define WHILE "WHILE"
#define DO "DO"
#define DONE "DONE"

#define IF "IF"
#define THEN "THEN"
#define ELSE "ELSE"
#define FI "FI"


/// THE FUNCTION handles entered if-line
//# RETURNS nothing
void Repl::if_handler(int argc, char** argv)
{
    /// check corectness
    if(argc < 4) {
        cout << "  incorrect command" << endl;
        return;
    }
    if(strcmp(argv[2],THEN)) {
        cout << "  " << THEN << " is absent" << endl;
        return;
    }
    if(strcmp(argv[argc-1],FI)) {
        cout << "  " << FI << " is absent" << endl;
        return;
    }
    
    
    /// calculate logical expression
    char** argv_cond = new char* [MAX_CMD_NUM];
    argv_cond[1] = argv[1];
    int argc_cond = logic_parser(argc, argv_cond);
    char* result = logic_calculator(argc_cond, argv_cond);
    if(result == NULL) {
        cout << "  incorrect logical expression" << endl;
        return;
    }
    
    
    /// find key work THEN and ELSE
    int i_then = 2;
    int i_else = 3;
    
    while(i_else < argc) {
        if(!strcmp(argv[i_else],ELSE))
            break;
        i_else++;        
    }
    if(i_else==argc)
        i_else--;
      
    
    /// if expression == TRUE, divide THEN lines through ';'
    if(!strcmp(result, "T")) {
        if((i_then+1) == i_else)
            return;
 
        int pos = i_then+1;
        if(!strcmp(argv[pos],";")) {
            cout << "  empty command" << endl;
            return;
        }
        
        while(1) {
            while(pos != i_else && strcmp(argv[pos],";") && strcmp(argv[pos],ELSE))
                pos++; 

            int argc_com = pos-i_then-1;
            char** argv_com = new char* [argc_com+1];
            int j = 0;
            for(int i = i_then+1; i < pos; i++)
                argv_com[j++] = argv[i];
            argv_com[j] = NULL;

            analyzer(argc_com, argv_com);
            //delete[] argv_com;
            
            i_then = pos;
            pos++;
            
            if(i_then == i_else)
                break;    
            if(i_then == (i_else-1))
                break; 
            
            if(!strcmp(argv[pos],";")) {
                pos++;
                i_then++;
            }
        }
    }
    /// if expression == TRUE, divide ELSE lines through ';'
    else { 
        if(i_else >= (argc-2))
            return;
            
        int pos = i_else + 1;
        if(!strcmp(argv[pos],";")) {
            cout << "  empty command" << endl;
            return;
        }
        
        while(1) {
            while(strcmp(argv[pos],";") && strcmp(argv[pos],FI))
                pos++; 
                
            int argc_com = pos-i_else-1;
            char** argv_com = new char* [argc_com+1];
            int j = 0;
            for(int i = i_else+1; i < pos; i++)
                argv_com[j++] = argv[i];
            argv_com[j] = NULL;

            analyzer(argc_com, argv_com);
            //delete[] argv_com;

            i_else = pos;
            pos++;
            
            if(i_else == (argc-1))
                break;
                
            if(!strcmp(argv[pos],";")) {
                pos++;
                i_else++;
            }
            
            if(i_else == (argc-2))
                break;
            
        }
    }
};


/// THE FUNCTION handles entered if-line
//# RETURNS nothing
void Repl::while_handler(int argc, char** argv)
{
    /// check corectness
    if(argc < 4) {
        cout << "  incorrect command" << endl;
        return;
    }
    if(strcmp(argv[2], DO)) {
        cout << "  " << DO << " is absent" << endl;
        return;
    }
    if(strcmp(argv[argc-1], DONE)) {
        cout << "  " << DONE << " is absent" << endl;
        return;
    }
    
    /// logical expression
    char* expression = argv[1];
    char** argv_com = new char* [argc];
    
    
    /// find key work DO and DONE
    int i_do = 2;
    int i_done = 3;
    while(i_done < argc) {
        if(!strcmp(argv[i_done], DONE))
            break;
        i_done++;        
    }
    
    /// check emtyness of command list
    if(i_do == (i_done-1))
        return;
        
    if(!strcmp(argv[i_do+1],";")) {
        cout << "  empty command" << endl;
        return;
    }
        
    
    /// while expression == TRUE
    while(1) { 
        char** argv_cond = new char* [MAX_CMD_NUM];
        argv_cond[1] = expression;
        int argc_cond = logic_parser(argc, argv_cond);
        char* result = logic_calculator(argc_cond, argv_cond); 

        if(result == NULL) {
            cout << "  incorrect logical expression" << endl;
            delete[] argv_cond;
            return;
        }
        if(!strcmp(result, "F")){
            delete[] argv_cond;
            return;
        }
        
        int pos = i_do + 1;
        
        /// execute all command between DO and DONE     
        while(1) {
            
            while(strcmp(argv[pos],";") && strcmp(argv[pos],DONE))
                pos++;  
                 
            int argc_com = pos-i_do-1;
            int j = 0;
            
            
            for(int i = i_do+1; i < pos; i++) {
                argv_com[j++] = argv[i];
            }
            argv_com[j] = NULL;
              
            analyzer(argc_com, argv_com);
              
            i_do = pos;
            pos++;
        
            if(i_do == (argc-1))
                break;
            
            if(!strcmp(argv[pos],";")) {
                pos++;
                i_do++;
            }
            
            if(i_do == (i_done-1))
                break;
        }
        i_do = 2;
        delete[] argv_cond;
    }
};


void Repl::for_handler(int argc, char** argv) 
{
    cout << "  this is FOR" << endl;
};