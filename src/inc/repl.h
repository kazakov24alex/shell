#pragma once

#include <cstdio>
#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

#define BUFFER_SIZE 256
#define HISTORY_SIZE 500
#define MAX_FILENAME 256
#define MAX_LEN_PATH 1024
#define MAX_CMD_NUM 256


class Repl 
{    
    char c[2];          // entered from the keyboard symbol
    char* cur;          // the current position of the cursor in the input line
    unsigned int len;   // the length of buffer of input line
    
    char path[MAX_LEN_PATH];    // path of start directory
    
    char buf[BUFFER_SIZE];      // buffer of input line
    char buf_copy[BUFFER_SIZE]; // copy of buffer of input line
    
    char history_file[MAX_FILENAME];     // location of history file
    int hist_count;         // index of the command in the history
    int history_index;      // index of the command lifted from the history
    char* command;              // buffer lifted command
    char *history_entries[HISTORY_SIZE*2];  // array of commands history

    char* autocomplete_array[256];
    char autocomplete_buf[8192];
    

public:
// repl.cpp    
    Repl();
    ~Repl();
    void control_char();
    bool scripts_handler(int argc, char** argv);
    void analyzer(int argc, char** argv);
    
// check.cpp
    bool command_chk(int argc, char** argv);
    bool if_chk(int argc, char** argv);
    bool while_chk(int argc, char** argv);
    bool for_chk(int argc, char** argv);
    bool math_chk(int argc, char** argv);
    bool exec_chk(int argc, char** argv);

// cycles.cpp
    void if_handler(int argc, char** argv);
    void while_handler(int argc, char** argv);
    void for_handler(int argc, char** argv);
    
// input_ctrl.cpp 
    void print_cwd();   
    void add_char();
    void cursor_left();
    void cursor_right();
    void erase_char();
    void remove_char_from_buffer();
    int parser_arg(char* buf, char** argv);
    void replace_buf(char* new_buf);
    
// history.cpp
    void history_init();
    void history_cmd(int argc, char** argv);
    void history_save_cmd(char* cmd);
    char* history_entry(int i);
    void history_lift(int key);
    
// autocomplete.cpp
    void autocomplete();
    char** autocomplete_find(char* piece, bool cd);    
    
// dir.cpp
    bool cd_cmd(int argc, char** argv);
    void ls_cmd(int argc, char** argv);
    void mkdir_cmd(int argc, char** argv);
    void touch_cmd(int argc, char** argv);
    
// help.cpp
    void help_cmd(int argc, char** argv);
    
// exec.cpp
    void exec_cmd(int argc, char** argv);

// file.cpp
    void cat_cmd(int argc, char** argv);
    void file_cmd(int argc, char** argv); 

// attributes.cpp
    int uid_gid_extract(char* name, bool users);
    void chmod_cmd(int argc, char** argv);
    void chown_cmd(int argc, char** argv);
    void chgrp_cmd(int argc, char** argv);
    
// variable.cpp
    void var_assign(int argc, char** argv);
    void var_erase(int argc, char** argv);
    void var_show(int argc, char** argv);
    char* var_value(char* name);
    bool var_delete(char* name);
    bool var_substitution(int argc, char** argv);

// math.cpp
    int math_parser(int argc, char** argv);
    int math_calculator(int argc, char** argv);
 
    bool math_syntax(int argc, char** argv, int* type);
    void math_transformation(int argc, char** argv, int* expr, int* type);
    bool math_substitution(int argc, char** argv, int* type);
    int math_calculation(int argc, int* expr, int* type);
    int math_operation(int index, int argc, int* expr, int* type);
    char* math_tostring(int var);
    
// logic.cpp
    int logic_parser(int argc, char** argv);
    char* logic_calculator(int argc, char** argv);
    
    bool logic_map(int argc, char** argv, int* type);
    bool logic_syntax(int argc, char** argv, int* type);
    char* logic_calculation(int argc, char** expr, int* type);

    int negat_operation(int index, int argc, char** expr, int* type);
    int logic_operation(int index, int argc, char** expr, int* type);
    int compar_operation(int i, int argc, char** expr, int* type);
};