#include "inc/repl.h"

#include <cstdio>
#include <iostream> 
#include <unistd.h> 
#include <cstring>
using namespace std;

#define MAX_CMD_LEN 256


/// This function explores the existence command in the line
//# and process it 
bool Repl::command_chk(int argc, char** argv)
{    
    int NUM_CMD = 11;
    char typecom[][MAX_CMD_LEN] = {"ls", "cd", "history", "help", "mkdir","touch", "cat", "file", "chmod", "chown", "chgrp"};
    enum enumcom {LS, CD, HISTORY, HELP, MKDIR, TOUCH, CAT, FILE, CHMOD, CHOWN, CHGRP};
    
    /// run the appropriate command function
    int num_cmd = 0;   // the number of function in the function list
    for(num_cmd = 0; num_cmd < NUM_CMD; num_cmd++) {
        if(strcmp(argv[0], typecom[num_cmd]) == 0) {
            //arr_cmd[num_cmd](argc, argv);
            break;
        }
    }

    switch(num_cmd)
    {
        case LS:
            ls_cmd(argc, argv);
            break;
        case CD:
            cd_cmd(argc, argv);
            break;
        case HISTORY:
            history_cmd(argc, argv);
            break;
        case HELP:
            help_cmd(argc, argv);
            break;
        case MKDIR:
            mkdir_cmd(argc, argv);
            break;
        case TOUCH:
            touch_cmd(argc, argv);
            break;
        case CAT:
            cat_cmd(argc, argv);
            break;
        case FILE:
            file_cmd(argc, argv);
            break;
        case CHMOD:
            chmod_cmd(argc, argv);
            break;
        case CHOWN:
            chown_cmd(argc, argv);
            break;
        case CHGRP:
            chgrp_cmd(argc, argv);
            break;
            /// if the team has not been found, understand the first argument 
            //# as the name of the executable file and try to run it
        default:
            return false;
            break;
    }
    return true;
};


/// THE FUNCTION explores the existence 'if' in the line
//# and process it 
bool Repl::if_chk(int argc, char** argv)
{
    if(!strcmp(argv[0],"IF")) {
        if_handler(argc, argv);
        return true;
    } else
        return false; 
};


/// This function explores the existence 'while' in the line
//# and process it 
bool Repl::while_chk(int argc, char** argv) 
{
    if(!strcmp(argv[0],"WHILE")) {
        while_handler(argc, argv);
        return true;
    } else
        return false; 
};


/// This function explores the existence 'for' in the line
//# and process it 
bool Repl::for_chk(int argc, char** argv)
{
    if(!strcmp(argv[0],"FOR")) {
        for_handler(argc, argv);
        return true;
    } else
        return false; 
};


/// THE FUNCTION explores the existence math symbols in the line
//# and process it ]
//# RETURNS true or false (line - a mathematical operation?)
bool Repl::math_chk(int argc, char** argv)
{
    bool math = false;  // indicator of math type of line

    /// if line = "<<" - print variables
    if(!strcmp(argv[0],"<<")) {
        var_show(argc, argv);
        return true;
    /// if line begins with "--" - delete a variables from  
    } else if(!strcmp(argv[0], "--")) {
        var_erase(argc, argv);
        return true;
    }

    /// if there is only one argument, check the line for the 
    //# presence of mathematical symbols
    if(argc == 1) {
        for(int i = 0; i < strlen(argv[0]); i++) 
            /// if there is math symbol, calculate this expression
            if(argv[0][i]=='+' || argv[0][i]=='-' || argv[0][i]=='*' || argv[0][i]=='+') {
                argv[2] = argv[0];
                argc = 3;
                /// parse argv[2] into mathematical component of expression
                //# argc - number of mathematical components
                //# argc==0 (expression is incorrect) - return NULL
                argc = math_parser(argc, argv);
                if(argc == 0)
                    return true;
                int res = math_calculator(argc, argv);
                if(argv[0]!=NULL)
                    cout << "  = " << res << endl;
                return true;
            }
    /// if line contains "=" - assign a value of expression to variable
    } else if(!strcmp(argv[1], "=")) {
        var_assign(argc, argv);   
        return true;
    }
    
    /// all the condition have not been - return not mathematical type        
    return false;
};   


/// This function try to run file from argument
bool Repl::exec_chk(int argc, char** argv)
{ 
        if(argc > 0) {    
            sleep(0.5);
            exec_cmd(argc, argv);
        }
};