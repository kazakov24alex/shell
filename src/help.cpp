#include "inc/repl.h"

#include <cstdio>
#include <iostream>
#include <unistd.h>
#include <cstring>
#include <stdlib.h>
#include <fstream>

#define MAX_STR_LEN 4096    // max length of line from help file


// The function displays a short help description of the program
void Repl::help_cmd(int argc, char** argv) {
    /// path to help file
	char help_path[] = "/usr/share/shell/help";
    
    /// open help file and read it, writting its line on screen
    char *str = new char [MAX_STR_LEN];
    ifstream f_help(help_path);
    if (f_help.is_open()) {
        while (!f_help.eof()) {
            f_help.getline(str, MAX_STR_LEN, '\n');
            cout << str << endl;
        }
    }
    else
        cout << "  Sorry, help is unavailable now." << endl;
    
    /// close help file
    f_help.close();
}