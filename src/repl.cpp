#include "inc/repl.h"

#include <cstdio>
#include <iostream> 
#include <unistd.h> 
#include <cstring>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
using namespace std;


#define MAX_CWD 256
#define MAX_CMD_LEN 256
#define MAX_CMD_NUM 256

#define PATH_NUM 25 
#define MAX_LEN_PATH 1024

#define KEY_BACKSPACE 8
#define KEY_DELETE 127
#define KEY_ARROWS 27
#define KEY_TAB 9
#define KEY_RETURN '\n'  
#define KEY_SC_UP 'A'
#define KEY_SC_DOWN 'B'
#define KEY_SC_RIGHT 'C'
#define KEY_SC_LEFT 'D'


/// THE FUNCTION is the main function of REPL
/// responsible for controlling the input line and processing the entered data
Repl::Repl()
{
    /// initialization of history
    hist_count = 0;
    history_init();
    
    /// announce list of commands and common symbols
    char eol = '\n';

    /// cycle of REPL
    while(1) {
        /// print invitation to entry 
        print_cwd();
        
        /// the initial values of the basic variables
        history_index = 0;
        cur = buf;
        *cur = '\0';
        len = 0;
        command = NULL;

        /// read entered character and its processing
        control_char();
        
        /// the completion of the input line (occurs after pressing enter)
        strcpy(buf_copy, buf);
        char eol = '\n';
		write(STDOUT_FILENO, &eol, 1);
		
        /// parse of completed input line
		char* argv[MAX_CMD_NUM];
		int argc = parser_arg(buf, argv);
        
        
        /// save the input line into history, if it was not empty
        if(argc > 0) {
            history_save_cmd(buf_copy);
        } else if(argc == 0) {
            continue;
        }
        
        /// close the program, if command "exit" had been 
        if(strcmp(argv[0], "exit") == 0)
            break;
  
        
        /// look for '&' (ap) in the entered line
        bool ap = false;
	    int k = 0;
        while(argv[k]!=NULL) {
		    if(!strcmp(argv[k], "&"))
			    ap = true;
		        k++;
	    }
        
        /// if there isn't script form - use analyzer
	    if(ap==false) {
		    analyzer(argc, argv); 
		    continue;
	    }
    
        /// if there isn't script form - use script handler
        if(scripts_handler(argc, argv) == false)
            continue; 
    }

};


Repl::~Repl() { };


/// THE FUNCTION analyze type of entered line and
//# it guides line for further processing 
//# RETURNS nothing
void Repl::analyzer(int argc, char** argv)
{  
    if(if_chk(argc, argv))
        return;
    if(while_chk(argc, argv))
        return;
    if(for_chk(argc, argv))
        return;
    if(math_chk(argc, argv))
        return;
     
    /// substitution of values of variables    
    if(var_substitution(argc, argv) == false)
        return;    
    if(command_chk(argc, argv))
        return;
    exec_chk(argc, argv);
    return;
}


/// THE FUNCTION control key presses
//# RETURNS nothing
void Repl::control_char()
{      
    while(read(STDIN_FILENO, &c, 1) != 0) {
        if(c[0] == KEY_RETURN) {
            break;
        }
        switch(c[0]) {
            case KEY_BACKSPACE:
            case KEY_DELETE:
                if (cur > buf)
                    erase_char();
                break;
            case KEY_ARROWS:
                read(STDIN_FILENO, &c, 2);
                if(c[0] == '[') {
                    switch(c[1]) {
                        case KEY_SC_UP:
                            history_lift(KEY_SC_UP);
                            break;
                        case KEY_SC_DOWN: 
                            history_lift(KEY_SC_DOWN);
                            break;
                        case KEY_SC_RIGHT:
                            cursor_right();
                            break;
                        case KEY_SC_LEFT:
                            cursor_left();
                            break;
                    }
                }
                break;
            case KEY_TAB:
                autocomplete();
                break;
            default:
                add_char();
                break;              
        }  
    } 
};


/// THE FUNCTION handle srcipts from entered line
//# RETURNS boolean for errors
bool Repl::scripts_handler(int argc, char** argv)
{
    char** argv_com = new char* [argc];    
    /// find key work DO and DONE
    int i_do = -1;
    int i_done = 1;
    while(i_done < argc) {
        if(!strcmp(argv[i_done], "&"))
            break;
        i_done++;        
    }
    
    /// check emtyness of command list
    if(i_do == (i_done-1)) {
        cout << "  syntax error (empty command)" << endl;
        return false;
    }
        
    if(!strcmp(argv[i_do+1],"&")) {
        cout << "  syntax error (empty command)" << endl;
        return false;
    }
        
    
    /// selection of commands from a script 
    int pos = i_do + 1;
    while(1) {
        while(strcmp(argv[pos],"&") && pos<(argc-1))
            pos++;  
            
        if(pos == (argc-1))
            pos++;
                 
        int argc_com = pos-i_do-1;
        int j = 0;
             
        for(int i = i_do+1; i < pos; i++)
            argv_com[j++] = argv[i];
        argv_com[j] = NULL;
            
        /// analyze the selected command    
        analyzer(argc_com, argv_com);
        
        /// check on the end of script    
        if(pos == (argc))
            break;
              
        i_do = pos;
        pos++;            
            
        if(!strcmp(argv[pos],"&")) {
            pos++;
            i_do++;
        }
                            
        if(i_do == (i_done-1))
            break;
    }
   
    return true;
}